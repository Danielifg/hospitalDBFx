package com.danielFlores.hospitalDB.unitTests;

import com.danielFlores.hospitalDB.beans.Patient;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_read;
import com.danielFlores.hospitalDB.beans.Inpatient;
import com.danielFlores.hospitalDB.beans.Medication;
import com.danielFlores.hospitalDB.beans.Surgical;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_create;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_update;

public class testDataBase {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final String url = "jdbc:mysql://localhost:3306/";
    private final String user = "root";
    private final String password = "";

    public Timestamp convertStringToTimestamp(String str_date) {
        try {
            DateFormat formatter;
            formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = (Date) formatter.parse(str_date);
            java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

            return timeStampDate;
        } catch (ParseException e) {
            log.info("Exception :" + e);
            return null;
        }
    }

    @Test
    public void createPatient() throws SQLException {
        HospitalDB_read C1 = new HospitalDB_read();
        HospitalDB_create CC = new HospitalDB_create();
        Patient p1 = new Patient(6, "Dany", "Bogottá", "Enlightment", "2014-01-23 9:00:00", "2014-01-23 9:00:00");

        CC.createPatient(p1);

        Patient p2 = C1.findRecordsById(6);
        //assertEquals(p1, p2);

        log.info(CC.createPatient(p1) + "PatientInsert");

    }

    @Test
    public void createInpatient() throws SQLException {

        HospitalDB_create C1 = new HospitalDB_create();
        Inpatient P1 = new Inpatient(6, 6, ("2014-01-23 9:00:00"), " ", 2.2, 2.2, 2.2);

        C1.createInpatient(P1);

        log.info(" number of row affected " + C1.createInpatient(P1));

    }

    @Test
    public void createMedication() throws SQLException {
        HospitalDB_create C1 = new HospitalDB_create();
        Medication P1 = new Medication(3, 3, ("2014-01-23 9:00:00"), "", 2.2, 2.2);

        C1.createMedication(P1);

        log.info(C1.createMedication(P1) + "MedicationInsert");

    }

    @Test
    public void createSurgical() throws SQLException {
        HospitalDB_create C1 = new HospitalDB_create();
        Surgical P1 = new Surgical(3, 3, "2014-01-23 9:00:00", "", 2.2, 2.2, 2.3);

        C1.createSurgical(P1);

        log.info(C1.createSurgical(P1) + "SurgicalInsert");

    }

    @Test
    public void readAllInpaients() throws SQLException {

        HospitalDB_read dAO = new HospitalDB_read();
        List<Inpatient> hospitalDBList = dAO.displayInpatientDB();
        //displayAll(ids); 
        assertEquals("# ids", 20, hospitalDBList.size());
        log.info("|||||" + hospitalDBList.size() + hospitalDBList.toString());

    }

    @Test
    public void readAllMedication() throws SQLException {

        HospitalDB_read dAO = new HospitalDB_read();
        List<Medication> hospitalDBList = dAO.displayMedicationDB();
        //displayAll(ids); 
        assertEquals("# ids", 5, hospitalDBList.size());
        log.info("|||||" + hospitalDBList.size() + hospitalDBList.toString());

    }

    @Test
    public void readAllSurgical() throws SQLException {

        HospitalDB_read dAO = new HospitalDB_read();
        List<Surgical> hospitalDBList = dAO.displaySurgicalDB();
        //displayAll(ids); 
        assertEquals("# ids", 5, hospitalDBList.size());
        log.info("|||||" + hospitalDBList.size() + hospitalDBList.toString());

    }

    @Test
    public void readFindRecordsbyID() throws SQLException {
        Patient p1 = new Patient(3, "Kent", "Clark", "Tonsilitis", "2014-05-02 09:00:00.0", "2014-05-07 13:00:00.0");

        HospitalDB_read dao = new HospitalDB_read();
        Patient p2 = dao.findRecordsById(3);
        log.info("testFindID for record one" + p2);
        assertEquals(p1, p2);
    }

    @Test
    public void readFindRecordsByLastName() throws SQLException {
        Patient p1 = new Patient(3, "Kent", "Clark", "Tonsilitis", "2014-05-02 09:00:00.0", "2014-05-07 13:00:00.0");

        HospitalDB_read dao = new HospitalDB_read();
        Patient p2 = dao.findRecordsByLastName("Kent");
        log.info("testFindByLastName for record one" + p2);
        assertEquals(p1, p2);
    }

    @Test
    public void readDisplayInpatientDB() throws SQLException {
        HospitalDB_read dAO = new HospitalDB_read();
        List<Inpatient> InpatientDBList = dAO.displayInpatientDB();
        assertEquals("#", 20, InpatientDBList.size());
        log.info("|||||" + InpatientDBList.size() + InpatientDBList.toString());

    }

    @Test
    public void readDisplaySurgicalDB() throws SQLException {
        HospitalDB_read dAO = new HospitalDB_read();
        List<Surgical> SurgicalDBList = dAO.displaySurgicalDB();
        assertEquals("#", 5, SurgicalDBList.size());
        log.info("|||||" + SurgicalDBList.size() + SurgicalDBList.toString());
    }

    @Test
    public void readDisplayMedicationDB() throws SQLException {
        HospitalDB_read dAO = new HospitalDB_read();
        List<Medication> MedicationDBList = dAO.displayMedicationDB();
        assertEquals("#", 5, MedicationDBList.size());
        log.info("|||||" + MedicationDBList.size() + MedicationDBList.toString());
    }

    // @Test
    public void readFindNextPatientByID() throws SQLException {
        Patient p3 = new Patient(3, "Kent", "Clark", "Tonsilitis", "2014-05-02 09:00:00.0", "2014-05-07 13:00:00.0");
        Patient p2 = new Patient(2, "Allen", "Barry", "Kidney Stones", "2014-02-18 09:00:00.0", "2014-02-21 13:00:00.0");

        HospitalDB_read dao = new HospitalDB_read();

        Patient nextt = dao.findNextPatientByID(p2);

        assertEquals("NEXT" + p3, nextt);

    }

    @Test
    public void readFindPreviousPatientByID() throws SQLException {
        Patient p3 = new Patient(3, "Kent", "Clark", "Tonsilitis", "2014-05-02 09:00:00.0", "2014-05-07 13:00:00.0");
        Patient p2 = new Patient(p3.getId(), "Allen", "Barry", "Kidney Stones", "2014-02-18 09:00:00.0", "2014-02-21 13:00:00.0");

        HospitalDB_read dao = new HospitalDB_read();
        Patient previous = dao.findPreviousPatientByID(p3);

        assertEquals("PREVIOUS: ", p2, previous);
    }

    //@Test
    public void updatePatient() throws SQLException {

        Patient p3 = new Patient(5, "KKK", "KKKK", "KKKKKKKKK", "2014-05-02 09:00:00.0", "2014-05-07 13:00:00.0");
        Patient p2 = new Patient(2, "Allen", "Barry", "Kidney Stones", "2014-02-18 09:00:00.0", "2014-02-21 13:00:00.0");

        HospitalDB_update dao_update = new HospitalDB_update();
        HospitalDB_read dao_read = new HospitalDB_read();

        dao_update.updatePatient(p3);
        Patient p1 = dao_read.findRecordsById(5);
        assertEquals(p1, p3);

    }

    public void delete() {
        //can't delete forein key, but extra added rows are deleted 
    }

    @Before
    public void seedDatabase() {
        final String seedDataScript = loadAsString("hospitaldb.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<String>();
        try {
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
}
