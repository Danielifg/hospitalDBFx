package com.danielFlores.hospitalDB.unitTests;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class logConfig extends TestWatcher {

 
    private Logger log;

    /**
     * Constructor
     */
    public logConfig() {
        super();
    }

    
    @Override
    protected void starting(Description description) {
        prepareLogger(description);
        log.info("Starting test [{}]", description.getMethodName());
    }

     
    @Override
    protected void failed(Throwable e, Description description) {
        log.info("Failed test [{}]", description.getMethodName() + "\n" + e.getMessage());
    }

  
    
    private void prepareLogger(Description description) {
        if (log == null) {
            log = LoggerFactory.getLogger(description.getClassName());
        }
    }
}
