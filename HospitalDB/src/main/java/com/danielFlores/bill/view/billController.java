package com.danielFlores.bill.view;

import com.danielFlores.hospitalDB.beans.BillReport;
import java.sql.SQLException;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_read;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_create;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_delete;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_update;
/**
 *
 * @author Daniel
 *
 * Class Controller for Billing UI Tab.
 *
 * Set the patient expenses, total and Print the patient Billing Report.
 *
 */
public class billController {

    private HospitalDB_read dao_read;
    private BillReport bill;
    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    @FXML
    private AnchorPane billScene;

    @FXML
    private TextField searcher;

    @FXML
    private Label idField;

    @FXML
    private Label nameField;

    @FXML
    private Label roomNumber;

    @FXML
    private Label dailyRateField;

    @FXML
    private Label suppliesField;

    @FXML
    private Label servicesField;

    @FXML
    private Label roomFeefield;

    @FXML
    private Label surgeOnField;

    @FXML
    private Label medicationField;

    @FXML
    private TextField totalField;

    @FXML
    private TextField patientName1;
    private HospitalDB_update dao_update;
    private HospitalDB_create dao_create;
    private HospitalDB_delete dao_delete;

    @FXML
    void exit(ActionEvent event) {

    }

    public billController() throws SQLException {
        super();
        bill = new BillReport();
    }

    /*
    Load the form with the query Results for the patient ID expenses.
     */
    private void setForm(BillReport bill) {
        Bindings.bindBidirectional(idField.textProperty(), bill.idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(roomNumber.textProperty(), bill.roomNumberProperty());
        Bindings.bindBidirectional(nameField.textProperty(), bill.lastNameProperty());
        Bindings.bindBidirectional(dailyRateField.textProperty(), bill.DailyRateProperty());
        Bindings.bindBidirectional(suppliesField.textProperty(), bill.SuppliesProperty());
        Bindings.bindBidirectional(servicesField.textProperty(), bill.ServicesProperty());
        Bindings.bindBidirectional(roomFeefield.textProperty(), bill.RoomFeeProperty());
        Bindings.bindBidirectional(surgeOnField.textProperty(), bill.surgeOnFeeProperty());
        Bindings.bindBidirectional(medicationField.textProperty(), bill.medicationProperty());
        Bindings.bindBidirectional(totalField.textProperty(), bill.totalProperty());
        Bindings.bindBidirectional(nameField.textProperty(), bill.lastNameProperty());

    }

    @FXML
    private void initialize() throws SQLException {
//                 setForm(bill);
//                setReference();
//                  //  showTable();
//                
    }

    public void setDao_create(HospitalDB_create dao_create) {
        this.dao_create = dao_create;
    }

    public void setDao_read(HospitalDB_read dao_read) {
        this.dao_read = dao_read;
    }

    public void setDao_update(HospitalDB_update dao_update) {
        this.dao_update = dao_update;
    }

    public void setDao_delete(HospitalDB_delete dao_delete) {
        this.dao_delete = dao_delete;
    }

    /*
          Method to search for the patient spendings.                
     */
    @FXML
    void searcher(ActionEvent event) throws SQLException {
        Alert dialog = new Alert(Alert.AlertType.WARNING);

        if (!searcher.getText().isEmpty()) {
            int id = Integer.valueOf(searcher.getText());
            bill = dao_read.displayReport(id);

            BillReport bill2 = dao_read.displayTotal(id);

            setForm(bill);
            Bindings.bindBidirectional(totalField.textProperty(), bill2.totalProperty());
        } else {
            dialog.setContentText("No Id found");
            dialog.show();
        }

    }

}
