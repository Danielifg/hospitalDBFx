
package com.danielFlores.hospitalApp;

import com.danielFlores.hospitalDB.view.rootLayoutController;
import java.io.IOException;
import java.util.Locale;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.danielFlores.hospitalDB.persistence.daoDeclaration.HospitalDAO_create;

/**
 *
 * @author Daniel
 *
 * Main Application UI.
 * Class to pull the root xml file in to the main scene.
 * 
 */
public class mainApp extends Application {

    private HospitalDAO_create hospitalDAO;
    private Stage primaryStage;
    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    private Locale currentLocale;

    private AnchorPane anchorPanes;

    public mainApp() {
        super();

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Hospital");

        initRootLayout();

        Scene scene = new Scene(anchorPanes);
        primaryStage.setScene(scene);
        primaryStage.show();

        log.info("Program Begins");
    }

    public void initRootLayout() {

        Locale locale = Locale.getDefault();
        log.debug("Locale = " + locale);
//		currentLocale = new Locale("en","CA");	
//		Locale currentLocale = Locale.CANADA;
//		Locale currentLocale = Locale.CANADA_FRENCH;
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(mainApp.class.getResource("/fxml/rootLayout.fxml"));
            anchorPanes = (AnchorPane) loader.load();
            // Show the scene containing the root layout.	                  
            rootLayoutController rootController = loader.getController();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
