/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielFlores.surgical.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 *
 * @author Daniel
 */
public abstract class formValidation {
       Alert dialog = new Alert(Alert.AlertType.WARNING);
     @FXML
    

    public boolean textFieldNumericValid(TextField i) {
        boolean num = false;
        if (i.getText().matches("[0-9]+")) {
            num = true;
        }
        return num;
    }

    public boolean textFieldNotEmptyValid(TextField i) {
        boolean valid = false;
        if (i.getText() != null & !i.getText().isEmpty()) {
            valid = true;
        }
       return valid;           
    }
    
    public String formValidation(TextField surgery,TextField roomFee, TextField surgeon,TextField supplies){
        String msg = "";
        
        if(surgery.getText().trim().matches("[0-9]")){
            msg="Surgery field not valid,";
        }
         if(!roomFee.getText().matches("[0-9-\\.,]+")){
            msg="Room Fee field not valid,";
        }
       
      
          if(!surgeon.getText().matches("[0-9-\\.,]+")){
            msg="Surgeon Fee field not valid,";
        }
            if(!supplies.getText().matches("[0-9-\\.,]+")){
            msg="Supplies field not valid,";
        }
        return  msg;
     }
    
}
