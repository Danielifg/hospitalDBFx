/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielFlores.surgical.view;

import com.danielFlores.hospitalDB.beans.Inpatient;
import com.danielFlores.hospitalDB.beans.Patient;
import com.danielFlores.hospitalDB.beans.Surgical;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.NumberStringConverter;
import com.danielFlores.hospitalDB.persistence.daoDeclaration.HospitalDAO_create;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_create;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_delete;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_read;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_update;

/**
 *
 * @author Daniel
 */
public class surgicalController extends formValidation {

    private Surgical surgical;
    private HospitalDB_read dao_read;
    private HospitalDB_create dao_create;
    private HospitalDB_update dao_update;
    private HospitalDB_delete dao_delete;

    @FXML
    private TextField searcher;
    @FXML
    private AnchorPane surgicalScene;

    @FXML
    private TextField idField;

    @FXML
    private TextField dateSurgeryField;

    @FXML
    private TextField surgeryField;

    @FXML
    private TextField roomFeeField;

    @FXML
    private TextField surgeOnFeeField;

    @FXML
    private TextField suppliesField;

    @FXML
    private TableView<Surgical> surgicalDB;

    @FXML
    private TableColumn<Patient, Number> C1;

    @FXML
    private TableColumn<Surgical, Number> C2;

    @FXML
    private TableColumn<Surgical, String> C3;

    @FXML
    private TableColumn<Surgical, String> C4;

    @FXML
    private TableColumn<Surgical, Number> C5;

    @FXML
    private TableColumn<Surgical, Number> C6;

    @FXML
    private TableColumn<Surgical, Number> C7;

    @FXML
    private TextField fieldID1;

    @FXML
    void fieldIDaction(ActionEvent event) {

    }

    public surgicalController() {
        super();
        surgical = new Surgical();
    }

    private void setTable() {

        // C1.setCellValueFactory(cellData -> cellData.getValue().idProperty());
        C2.setCellValueFactory(cellData -> cellData.getValue().patient_idProperty());
        C3.setCellValueFactory(cellData -> cellData.getValue().surgeryDateOFProperty());
        C4.setCellValueFactory(cellData -> cellData.getValue().surgeryProperty());
        C5.setCellValueFactory(cellData -> cellData.getValue().roomFeeProperty());
        C6.setCellValueFactory(cellData -> cellData.getValue().surgeOnFeeProperty());
        C7.setCellValueFactory(cellData -> cellData.getValue().supppliesProperty());
    }

    private void clearForm() {
        idField.setText("");
        dateSurgeryField.setText("");
        surgeryField.setText("");
        roomFeeField.setText("");
        surgeOnFeeField.setText("");
        suppliesField.setText("");
    }

    private void setForm(Surgical surgical) {

        Bindings.bindBidirectional(idField.textProperty(), surgical.patient_idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(dateSurgeryField.textProperty(), surgical.surgeryDateOFProperty());
        Bindings.bindBidirectional(surgeryField.textProperty(), surgical.surgeryProperty());
        Bindings.bindBidirectional(roomFeeField.textProperty(), surgical.roomFeeProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(surgeOnFeeField.textProperty(), surgical.surgeOnFeeProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(suppliesField.textProperty(), surgical.supppliesProperty(), new NumberStringConverter());

    }

    private void showTable() {
        surgicalDB.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showSurgicalDetails(newValue));
    }

    @FXML
    private void initialize() {
        setTable();
        setForm(surgical);
        showTable();

    }

    public void setHospitalDAO(HospitalDB_read dao_read) throws SQLException {
        this.dao_read = dao_read;
        dao_read.findNextSurgicalByID(surgical);
    }

    public void displayTheTable() throws SQLException {
        // Add observable list data to the table
        surgicalDB.setItems(this.dao_read.displaySurgicalDB());

    }

    private void showSurgicalDetails(Surgical surgical) {
        System.out.println(surgical);
    }

    @FXML
    void clear(ActionEvent event) {
        clearForm();
    }

    @FXML
    void next(ActionEvent event) throws SQLException {
        dao_read.findNextSurgicalByID(surgical);
    }

    @FXML
    void previous(ActionEvent event) throws SQLException {
        dao_read.findPreviousSurgicalByID(surgical);
    }

//****  Save Button is handling every Formfield validation *****//
    //******************************************************//
    private void saveForm() throws SQLException {

        if (textFieldNotEmptyValid(idField) & textFieldNumericValid(idField)) {

            if (formValidation(surgeryField, roomFeeField, surgeOnFeeField, suppliesField).equalsIgnoreCase("")) {
                Double roomFee = Double.valueOf(roomFeeField.getText().replaceAll("[\\,\\.]", ""));

                Double surge = Double.valueOf(surgeOnFeeField.getText().replaceAll("[\\,\\.]", ""));
                Double supplies = Double.valueOf(suppliesField.getText().replaceAll("[\\,\\.]", ""));
                int id = Integer.valueOf(idField.getText());
                if (id <= surgicalDB.getItems().size()) {

                    Surgical surgUp = new Surgical(id, id, dateSurgeryField.getText(), surgeryField.getText(), roomFee, surge, supplies);

                    dao_update.updateSurgical(surgUp);
                    displayTheTable();

                } else if (id > surgicalDB.getItems().size()) {
                    dialog.setContentText("Insert Id's on Patient Tab");
                    dialog.show();

                }
            } else {
                dialog.setContentText(formValidation(surgeOnFeeField, roomFeeField, surgeOnFeeField, suppliesField));
                dialog.show();
            }
        } else {
            dialog.setContentText("invalid ID");
            dialog.show();
        }
    }

    @FXML
    void save(ActionEvent event) throws SQLException {
        saveForm();
    }

    private void finder() {

        if (searcher.getText().isEmpty()) {
            dialog.setTitle("Invalid Hours");
            dialog.showAndWait();
        } else {

            try {
                int id = Integer.valueOf(searcher.getText());
                surgical = dao_read.findSurgicalById(id);
                setForm(surgical);

            } catch (Exception e) {
                dialog.setTitle("Invalid ID");
                dialog.setHeaderText("No Record Found");
                dialog.setContentText("Please try again!");
                dialog.showAndWait();
            }
        }
    }

    @FXML
    void searcherAction(ActionEvent event) {

        finder();
    }

    @FXML
    void delete(ActionEvent event) {
    }

    @FXML
    void exit(ActionEvent event) {
        Platform.exit();
    }
}
