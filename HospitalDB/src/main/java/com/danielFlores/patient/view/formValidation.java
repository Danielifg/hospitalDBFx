/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielFlores.patient.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 *
 * @author Daniel
 */
public abstract class formValidation {

    Alert dialog = new Alert(Alert.AlertType.WARNING);
     @FXML
    

    public boolean textFieldNumericValid(TextField i) {
        boolean num = false;
        if (i.getText().matches("[0-9]+")) {
            num = true;
        }
        return num;
    }

    public boolean textFieldNotEmptyValid(TextField i) {
        boolean valid = false;
        if (i.getText() != null & !i.getText().isEmpty()) {
            valid = true;
        }
       return valid;           
    }
    
    public String formValidation(TextField lastName,TextField firstName,TextField diagnosis){
        String msg = "";
        
        if(textFieldNumericValid(lastName)){
            msg="lastName not valid,";
        }
        if(textFieldNumericValid(firstName)){
            msg="gfirstName not valid,";
        }
        if(textFieldNumericValid(diagnosis)){
            msg="diagnosis not valid,";
        }
        return  msg;
     }
   

}
