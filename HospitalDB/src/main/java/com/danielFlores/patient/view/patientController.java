/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielFlores.patient.view;

import com.danielFlores.hospitalDB.beans.Inpatient;
import com.danielFlores.hospitalDB.beans.Patient;
import java.awt.event.InputMethodEvent;
import java.sql.SQLException;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_create;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_delete;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_read;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_update;

/**
 *
 * @author Daniel
 */
public class patientController extends formValidation {

    private Patient patient;
    private HospitalDB_read dao_read;
    private HospitalDB_create dao_create;
    private HospitalDB_update dao_update;
    private HospitalDB_delete dao_delete;

    Alert dialog = new Alert(Alert.AlertType.WARNING);
    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    @FXML
    private TextField searcher;

    @FXML
    private TextField fieldID;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField firstNameField;

    @FXML
    protected TextField diagnosisField;

    @FXML
    private TextField admissionDateField;

    @FXML
    private TextField releaseDateField;

    @FXML
    private AnchorPane patientAnchor;

    @FXML
    private TableView<Patient> HospitalDB;

    @FXML
    private TableColumn<Patient, Number> C1;

    @FXML
    private TableColumn<Patient, String> C2;

    @FXML
    private TableColumn<Patient, String> C3;

    @FXML
    private TableColumn<Patient, String> C4;

    @FXML
    private TableColumn<Patient, String> C5;

    @FXML
    private TableColumn<Patient, String> C6;

    public patientController() {
        super();
        patient = new Patient();
    }

    public IntegerProperty getPatientID() {
        return patient.idProperty();

    }

    private void setForm(Patient patient) {
        Bindings.bindBidirectional(fieldID.textProperty(), patient.idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(lastNameField.textProperty(), patient.lastNameProperty());
        Bindings.bindBidirectional(firstNameField.textProperty(), patient.firstNameProperty());
        Bindings.bindBidirectional(diagnosisField.textProperty(), patient.diagnosisProperty());
        Bindings.bindBidirectional(admissionDateField.textProperty(), patient.admissionDateProperty());
        Bindings.bindBidirectional(releaseDateField.textProperty(), patient.releaseDateProperty());
    }

    private void clearForm() {
        fieldID.setText("");
        lastNameField.setText("");
        firstNameField.setText("");
        diagnosisField.setText("");
        admissionDateField.setText("");
        releaseDateField.setText("");
    }

    private void setTable() {

//        String S = new SimpleDateFormat("MM/dd/yyyy").format(patient.admissionDateProperty());
        C1.setCellValueFactory(cellData -> cellData.getValue().idProperty());
        C2.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        C3.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        C4.setCellValueFactory(cellData -> cellData.getValue().diagnosisProperty());
        C5.setCellValueFactory(cellData -> cellData.getValue().admissionDateProperty());
        C6.setCellValueFactory(cellData -> cellData.getValue().releaseDateProperty());
    }

    private void showTable() {
        // Listen for selection changes and show the beans details when changed.
        HospitalDB.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showPatientDetails(newValue));
    }

    @FXML
    private void initialize() {

        setForm(patient);
        setTable();
        showTable();

    }

    public void setHospitalDAO(HospitalDB_read dao_read) throws SQLException {
        this.dao_read = dao_read;
        dao_read.findNextPatientByID(patient);
    }

    public void displayTheTable() throws SQLException {
        // Add observable list data to the table
        HospitalDB.setItems(this.dao_read.findAllRecords());
    }

    private void showPatientDetails(Patient patient) {
        // log.info(patient);
    }
    //***** Reference to the

    public TableView<Patient> getPatientDataTable() {
        return HospitalDB;
    }

    private void finder() {

        if (searcher.getText().isEmpty()) {
            dialog.setTitle("Invalid Hours");
            dialog.showAndWait();
        } else {
            try {
                try {
                    int id = Integer.valueOf(searcher.getText());
                    patient = dao_read.findRecordsById(id);
                    setForm(patient);
                } catch (NumberFormatException | SQLException e) {
                    Patient pa = dao_read.findRecordsByLastName(searcher.getText());
                    setForm(pa);
                }
            } catch (Exception e) {
                dialog.setTitle("Invalid Hours");
                dialog.setHeaderText("Hours Less Than Zero or Greater Than 744");
                dialog.setContentText("Please enter hours between 0 and 744.");
                dialog.showAndWait();
            }
        }
    }

    @FXML
    void searcher(ActionEvent event) throws SQLException {
        finder();
    }

    @FXML
    void clearForm(ActionEvent event) throws SQLException {
        clearForm();
    }

    private void deleteIt() throws SQLException {
        int id = Integer.valueOf(fieldID.getText());
        if (!fieldID.getText().isEmpty()) {

            if (id <= HospitalDB.getItems().size()) {

                dao_delete.deleteMaster(id);
                dao_delete.deleteInpatient(id);
                dao_delete.deleteMedication(id);
                dao_delete.deleteSurgical(id);

            }

        }
        displayTheTable();
        clearForm();
        log.info("deleted", dao_delete.deleteMaster(id));
    }

    @FXML
    void delete(ActionEvent event) throws SQLException {
        deleteIt();

    }

    @FXML
    void nextPatient(ActionEvent event) throws SQLException {
        dao_read.findNextPatientByID(patient);
    }

    @FXML
    void previousPatient(ActionEvent event) throws SQLException {
        dao_read.findPreviousPatientByID(patient);

    }

//****  Save Button is handling every Formfield validation *****//
    //******************************************************//
    @FXML
    void save(ActionEvent event) throws SQLException {

        if (textFieldNotEmptyValid(fieldID) & textFieldNumericValid(fieldID)) {

            if (formValidation(lastNameField, firstNameField, diagnosisField).equalsIgnoreCase("")) {

                int id = Integer.valueOf(fieldID.getText());
                if (id <= HospitalDB.getItems().size()) {

                    Patient update = new Patient(id, lastNameField.getText(), firstNameField.getText(),
                            diagnosisField.getText(), admissionDateField.getText(),
                            releaseDateField.getText());

                    dao_update.updatePatient(update);
                    displayTheTable();

                } else if (id > HospitalDB.getItems().size()) {

                    Patient insert = new Patient(id, lastNameField.getText(), firstNameField.getText(),
                            diagnosisField.getText(), admissionDateField.getText(),
                            releaseDateField.getText());
                    Inpatient inp1 = new Inpatient(id, id, " ", " ", 0.0, 0.0, 0.0);

                    dao_create.createPatient(insert);
                    dao_create.createInpatient(inp1);
                    displayTheTable();
                }
            } else {
                dialog.setContentText(formValidation(lastNameField, firstNameField, diagnosisField));
                dialog.show();
            }
        } else {
            dialog.setContentText("invalid ID");
            dialog.show();
        }
    }

    private void showPatientID(List<Patient> patientData) {

    }

    /**
     *
     * @param event
     */
    @FXML
    public void textChanged(InputMethodEvent event) {

        // how do I handle this"??
    }

    @FXML
    void exit(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void idFieldAction(ActionEvent event) {

    }

}
