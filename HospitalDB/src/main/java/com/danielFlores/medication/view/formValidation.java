/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielFlores.medication.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 *
 * @author Daniel
 */
public abstract class formValidation {
          Alert dialog = new Alert(Alert.AlertType.WARNING);
     @FXML
    

    public boolean textFieldNumericValid(TextField i) {
        boolean num = false;
        if (i.getText().matches("[0-9]+")) {
            num = true;
        }
        return num;
    }

    public boolean textFieldNotEmptyValid(TextField i) {
        boolean valid = false;
        if (i.getText() != null & !i.getText().isEmpty()) {
            valid = true;
        }
       return valid;           
    }
    
    public String formValidation(TextField medication,TextField units, TextField unitCost){
        String msg = "";
        
        if(medication.getText().trim().matches("[0-9]")){
            msg="Medication field not valid,";
        }
         if(!units.getText().matches("[0-9-\\.,]+")){
            msg="Units field not valid,";
        }
       
      
          if(!unitCost.getText().matches("[0-9-\\.,]+")){
            msg="UnitCost field not valid,";
          }
        return  msg;
     }
    
}
