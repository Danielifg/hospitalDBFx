/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielFlores.medication.view;

import com.danielFlores.hospitalDB.beans.Medication;
import com.danielFlores.hospitalDB.beans.Patient;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.NumberStringConverter;
import com.danielFlores.hospitalDB.persistence.daoDeclaration.HospitalDAO_create;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_create;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_delete;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_read;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_update;

/**
 *
 * @author Daniel
 */
public class medicationController extends formValidation {

    private Medication medication;
    private HospitalDB_read dao_read;
    private HospitalDB_create dao_create;
    private HospitalDB_update dao_update;
    private HospitalDB_delete dao_delete;

    @FXML
    private TextField searcher;
    @FXML
    private AnchorPane medicationScene;

    @FXML
    private TextField idField;

    @FXML
    private TextField medicationDateField;

    @FXML
    private TextField medicationField;

    @FXML
    private TextField unitsField;

    @FXML
    private TextField unitCostField;

    @FXML
    private TableView<Medication> medicationDB;

    @FXML
    private TableColumn<Patient, Number> C1;

    @FXML
    private TableColumn<Medication, Number> C2;

    @FXML
    private TableColumn<Medication, String> C3;

    @FXML
    private TableColumn<Medication, String> C4;

    @FXML
    private TableColumn<Medication, Number> C5;

    @FXML
    private TableColumn<Medication, Number> C6;

    @FXML
    private TextField fieldID1;

    @FXML
    void fieldIDaction(ActionEvent event) {

    }

    public medicationController() {
        super();
        medication = new Medication();
    }

    private void setForm(Medication medication) {

        Bindings.bindBidirectional(idField.textProperty(), medication.patientiD(), new NumberStringConverter());
        Bindings.bindBidirectional(medicationDateField.textProperty(), medication.dateOfMedProperty());
        Bindings.bindBidirectional(medicationField.textProperty(), medication.medProperty());
        Bindings.bindBidirectional(unitsField.textProperty(), medication.medUnitsProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(unitCostField.textProperty(), medication.unitCostProperty(), new NumberStringConverter());

    }

    private void setTable() {
        C2.setCellValueFactory(cellData -> cellData.getValue().patientiD());
        C3.setCellValueFactory(cellData -> cellData.getValue().dateOfMedProperty());
        C4.setCellValueFactory(cellData -> cellData.getValue().medProperty());
        C5.setCellValueFactory(cellData -> cellData.getValue().medUnitsProperty());
        C6.setCellValueFactory(cellData -> cellData.getValue().unitCostProperty());

    }

    private void showTable() {
        medicationDB.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showMedicationDetails(newValue));
    }

    @FXML
    private void initialize() {
        setForm(medication);
        setTable();
        showTable();

    }

    public void setHospitalDAO(HospitalDB_read dao_read) throws SQLException {
        this.dao_read = dao_read;
        dao_read.findNextMedicationByID(medication);
    }

    public void displayTheTable() throws SQLException {
        // Add observable list data to the table
        medicationDB.setItems(this.dao_read.displayMedicationDB());

    }

    private void showMedicationDetails(Medication medication) {
        System.out.println(medication);
    }

    @FXML
    void clear(ActionEvent event) {
        idField.setText("");
        medicationDateField.setText("");
        medicationField.setText("");
        unitsField.setText("");
        unitCostField.setText("");
    }

    @FXML
    void delete(ActionEvent event) {

    }

    @FXML
    void next(ActionEvent event) throws SQLException {
        dao_read.findNextMedicationByID(medication);
    }

    @FXML
    void previous(ActionEvent event) throws SQLException {
        dao_read.findPreviousMedicationtByID(medication);
    }

    private void saveIt() throws SQLException {

        if (textFieldNotEmptyValid(idField) & textFieldNumericValid(idField)) {
            try {
                Double surge = Double.valueOf(unitCostField.getText().replaceAll("[\\,\\.]", ""));
                Double supplies = Double.valueOf(unitsField.getText().replaceAll("[\\,\\.]", ""));
                int id = Integer.valueOf(idField.getText());
                Medication medUp = new Medication(id, id, medicationDateField.getText(), medicationField.getText(), surge, supplies);
                dao_update.updateMedication(medUp);
                displayTheTable();
            } catch (NumberFormatException e) {
                dialog.setContentText("Sorry but check your fields invalid");
                dialog.show();
            }

        } else {
            dialog.setContentText("Insert ID on Patient Tab");
            dialog.show();
        }
    }

    @FXML
    void save(ActionEvent event) throws SQLException {
        saveIt();
    }

    private void finder() {
        if (searcher.getText().isEmpty()) {
            dialog.setTitle("Invalid Hours");
            dialog.showAndWait();
        } else {
            try {

                int id = Integer.valueOf(searcher.getText());
                medication = dao_read.findMedicationById(id);
                setForm(medication);

            } catch (Exception e) {
                dialog.setTitle("Invalid Record");
                dialog.setHeaderText("Invalid ID");
                dialog.setContentText("Please try again!");
                dialog.showAndWait();
            }
        }
    }

    @FXML
    void searcherField(ActionEvent event) {
        finder();
    }

    @FXML
    void exit(ActionEvent event) {
        Platform.exit();
    }
}
