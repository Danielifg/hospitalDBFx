/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielFlores.inpatient.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 *
 * @author Daniel
 */
public abstract class formValidation {
      Alert dialog = new Alert(Alert.AlertType.WARNING);
     @FXML
    

    public boolean textFieldNumericValid(TextField i) {
        boolean num = false;
        if (i.getText().matches("[0-9]+")) {
            num = true;
        }
        return num;
    }

    public boolean textFieldNotEmptyValid(TextField i) {
        boolean valid = false;
        if (i.getText() != null & !i.getText().isEmpty()) {
            valid = true;
        }
       return valid;           
    }
    
    public String formValidation(TextField dailyRate,TextField dateOfStay, TextField supplies,TextField services){
        String msg = "";
        
        if(dateOfStay.getText().matches("[a-zA-z]")){
            msg="date of stay field not valid,";
        }
         if(!dailyRate.getText().matches("[0-9-\\.]+")){
            msg="daily Rate field not valid,";
        }
       
      
          if(!supplies.getText().matches("[0-9-\\.]+")){
            msg="supplies field not valid,";
        }
            if(!services.getText().matches("[0-9-\\.]+")){
            msg="services field not valid,";
        }
        return  msg;
     }
   
}
