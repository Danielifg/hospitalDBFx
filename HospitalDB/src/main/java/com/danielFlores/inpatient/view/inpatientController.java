package com.danielFlores.inpatient.view;

import com.danielFlores.hospitalDB.beans.Inpatient;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.NumberStringConverter;
import javafx.scene.control.Alert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_create;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_delete;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_read;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_update;

public class inpatientController extends formValidation {

    private Inpatient inpatient;
    Alert dialog = new Alert(Alert.AlertType.WARNING);
    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

//  private Patient patient;
    private HospitalDB_read dao_read;
    private HospitalDB_create dao_create;
    private HospitalDB_update dao_update;
    private HospitalDB_delete dao_delete;

    @FXML
    private TextField searcher;

    @FXML
    private AnchorPane inpatientScene;

    private TextField fieldID;

    @FXML
    private TextField inpatientID;

    @FXML
    private TextField dateOfStayField;

    @FXML
    private TextField roomNumberField;

    @FXML
    private TextField dailyRateField;

    @FXML
    private TextField suppliesField;

    @FXML
    private TextField servicesField;

    @FXML
    private TableView<Inpatient> inpatientDB;

    @FXML
    private TableColumn<Inpatient, Number> C2;

    @FXML
    private TableColumn<Inpatient, String> C3;

    @FXML
    private TableColumn<Inpatient, String> C4;

    @FXML
    private TableColumn<Inpatient, Number> C5;

    @FXML
    private TableColumn<Inpatient, Number> C6;

    @FXML
    private TableColumn<Inpatient, Number> C7;

    public inpatientController() {
        super();
        inpatient = new Inpatient();

    }

    @FXML
    private void clearForm() {
        inpatientID.setText("");
        dateOfStayField.setText("");
        roomNumberField.setText("");
        dailyRateField.setText("");
        suppliesField.setText("");
        servicesField.setText("");
    }

    private void setForm(Inpatient inpatient) {

        Bindings.bindBidirectional(inpatientID.textProperty(), inpatient.inPatientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(dateOfStayField.textProperty(), inpatient.dateOfStayProperty());
        Bindings.bindBidirectional(roomNumberField.textProperty(), inpatient.roomNumberProperty());
        Bindings.bindBidirectional(dailyRateField.textProperty(), inpatient.dailyRateProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(suppliesField.textProperty(), inpatient.suppliesProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(servicesField.textProperty(), inpatient.servicesProperty(), new NumberStringConverter());

    }

    private void setTable() {

        C2.setCellValueFactory(cellData -> cellData.getValue().inPatientIDProperty());
        C3.setCellValueFactory(cellData -> cellData.getValue().dateOfStayProperty());
        C4.setCellValueFactory(cellData -> cellData.getValue().roomNumberProperty());
        C5.setCellValueFactory(cellData -> cellData.getValue().dailyRateProperty());
        C6.setCellValueFactory(cellData -> cellData.getValue().suppliesProperty());
        C7.setCellValueFactory(cellData -> cellData.getValue().servicesProperty());

    }

    private void showTable() {
        // Listen for selection changes and show the beans details when changed.
        inpatientDB.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showInpatientDetails(newValue));
    }
    
    @FXML
    private void initialize() {
        setForm(inpatient);
        setTable();
        showTable();
    }

    public void setHospitalDAO(HospitalDB_read dao_read) throws SQLException {
        this.dao_read = dao_read;
        dao_read.findNextInpatientByID(inpatient);
    }

    public void displayTheTable() throws SQLException {
        // Add observable list data to the table
        inpatientDB.setItems(this.dao_read.displayInpatientDB());
    }

    private void showInpatientDetails(Inpatient inpatient) {
        System.out.println(inpatient);
    }

    void clearForm(ActionEvent event) {
        clearForm();
    }

    private void deleteIt() throws SQLException {
        int id = Integer.valueOf(fieldID.getText());
        if (!fieldID.getText().isEmpty()) {

            if (id <= inpatientDB.getItems().size()) {

                dao_delete.deleteInpatient(id);

            }

        }
        displayTheTable();
        clearForm();
        log.info("deleted", dao_delete.deleteMaster(id));
    }

    @FXML
    void delete(ActionEvent event) throws SQLException {
        deleteIt();
    }

    @FXML
    void fieldIDaction(ActionEvent event) {

    }

    @FXML
    void nextInpatient(ActionEvent event) throws SQLException {
        dao_read.findNextInpatientByID(inpatient);
    }

    @FXML
    void previousInpatient(ActionEvent event) throws SQLException {
        dao_read.findPreviousInpatientByID(inpatient);
    }

    private void saveMethod() throws SQLException {

        if (!inpatientID.getText().equalsIgnoreCase("")) {

            if (formValidation(dailyRateField, dateOfStayField, suppliesField, servicesField).equalsIgnoreCase("")) {

                int id = Integer.valueOf(inpatientID.getText());

                double dailyRate = Double.valueOf(dailyRateField.getText());
                double supplies = Double.valueOf(suppliesField.getText());
                double services = Double.valueOf(servicesField.getText());

                if (id <= inpatientDB.getItems().size()) {

                    Inpatient update = new Inpatient(id, id, dateOfStayField.getText(), roomNumberField.getText(), dailyRate, supplies, services);

                    dao_update.updateInpatient(update);
                    displayTheTable();

                } else if (id > inpatientDB.getItems().size()) {

                    dialog.setContentText("Insert Id's on Patient Tab");
                    dialog.show();
                }
            } else {
                dialog.setContentText(formValidation(dailyRateField, dateOfStayField, suppliesField, servicesField));
                dialog.show();
            }
        } else {
            dialog.setContentText("invalid ID");
            dialog.show();
        }
    }

    @FXML
    void save(ActionEvent event) throws SQLException {

        saveMethod();
    }

    private void finder() {
        if (searcher.getText().isEmpty()) {
            dialog.setTitle("Invalid Hours");
            dialog.showAndWait();
        } else {
            try {

                int id = Integer.valueOf(searcher.getText());
                inpatient = dao_read.findInpatientById(id);
                setForm(inpatient);

            } catch (Exception e) {
                dialog.setTitle("Invalid Record");
                dialog.setHeaderText("Invalid ID");
                dialog.setContentText("Please try again.");
                dialog.showAndWait();
            }
        }
    }

    @FXML
    void searcherAction(ActionEvent event) {
        finder();

    }

    @FXML
    void exit(ActionEvent event) {
        Platform.exit();
    }

}
