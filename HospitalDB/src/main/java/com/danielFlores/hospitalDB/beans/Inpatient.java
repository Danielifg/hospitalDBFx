package com.danielFlores.hospitalDB.beans;

import com.danielFlores.hospitalDB.view.rootLayoutController;
import java.util.Objects;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;



/**
 *
 * @author Daniel
 * 
 * POJO Inpatient refers to a an ex patient who's registered in DB.
 * 
 */
public class Inpatient  {
     
     private IntegerProperty PatientID;
     private IntegerProperty inPatientID;
     private StringProperty dateOfStay;
     private StringProperty roomNumber;
     private DoubleProperty dailyRate;
     private DoubleProperty supplies;
     private DoubleProperty services;
     private ObservableList<Inpatient> listOfInpatients;

   
     public Inpatient(){
           this(-1,-1,""," ",0.0,0.0,0.0);
     }

    public Inpatient(final int PatientID,final int inPatientID,final String dateOfStay,final String roomNumber, final double dailyRate,final double supplies, final double services) {
        super();
        
        this.PatientID = new SimpleIntegerProperty(PatientID);
        this.inPatientID = new SimpleIntegerProperty(inPatientID);
        this.dateOfStay =new SimpleStringProperty(dateOfStay);
        this.roomNumber = new SimpleStringProperty(roomNumber);
        this.dailyRate = new SimpleDoubleProperty(dailyRate);
        this.supplies = new SimpleDoubleProperty(supplies);
        this.services = new SimpleDoubleProperty(services);
    }
    
         public int getPatientID() {
        return PatientID.get();
    }

    public void setPatientID(final int PatientID) {
        this.PatientID.set(PatientID);
    }

    public IntegerProperty PatientIDProperty() {
        return PatientID;
    }
    
    
      public int getInpatientID() {
        return inPatientID.get();
    }

    public void setInpatientID(final int inPatientID) {
        this.inPatientID.set(inPatientID);
    }

    public IntegerProperty inPatientIDProperty() {
        return inPatientID;
    }

    public String getDateOfStay() {
        return dateOfStay.get();
    }

    public void setDateOfStay(String dateOfStay) {
        this.dateOfStay.set(dateOfStay);
    }
    
     public StringProperty dateOfStayProperty() {
        return dateOfStay;
    }
    
    
      public String getRoomNumber() {
        return roomNumber.get();
    }

    public void setRoomNumber(final String roomNumber) {
        this.roomNumber.set(roomNumber);
    }

    public StringProperty roomNumberProperty() {
        return roomNumber;
    }
    
      public double getDailyRate() {
        return dailyRate.get();
    }

    public void setdailyRate(final double dailyRate) {
        this.dailyRate.set(dailyRate);
    }

    public DoubleProperty dailyRateProperty() {
        return dailyRate;
    }
         public double getSupplies() {
        return supplies.get();
    }

    public void setSupplies(final double supplies) {
        this.supplies.set(supplies);
    }

    public DoubleProperty suppliesProperty() {
        return supplies;
    }
         public double getServices() {
        return services.get();
    }

    public void setServices(final double services) {
        this.services.set(services);
    }

    public DoubleProperty servicesProperty() {
        return services;
    }

    @Override
    public String toString() {
        return "Inpatient{" + "PatientID=" + PatientID + ", inPatientID=" + inPatientID + ", dateOfStay=" + dateOfStay + ", roomNumber=" + roomNumber + ", dailyRate=" + dailyRate + ", supplies=" + supplies + ", services=" + services + ", listOfInpatients=" + listOfInpatients + '}';
    }


    
    
     @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((roomNumber.get() == null) ? 0 : roomNumber.get().hashCode());
      
        
        return result;
    }
    
    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Inpatient other = (Inpatient) obj;
        
        if (!Objects.equals(this.PatientID.get(), other.PatientID.get())) {
            return false;
          }
        if (!Objects.equals(this.inPatientID.get(), other.inPatientID.get())) {
            return false;
        }
        if (!Objects.equals(this.dateOfStay.get(), other.dateOfStay.get())) {
            return false;
        }
        if (!Objects.equals(this.roomNumber.get(), other.roomNumber.get())) {
            return false;
        }
        if (!Objects.equals(this.dailyRate.get(), other.dailyRate.get())) {
            return false;
        }
        if (!Objects.equals(this.supplies.get(), other.supplies.get())) {
            return false;
        }
        if (!Objects.equals(this.services.get(), other.services.get())) {
            return false;
        }
        return true;
    }
    
    
}
    