
package com.danielFlores.hospitalDB.beans;

import java.util.Objects;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


/**
 *
 * @author Daniel
 * 
 * POJO to handle the total amount of patient expenses.
 * 
 */
public class BillReport {    
    private IntegerProperty patientID;
    private StringProperty lastName;
    private StringProperty firstName;
    private StringProperty dailyRate;
    private StringProperty supplies;
    private StringProperty ssupplies;
    private StringProperty services;
    private StringProperty roomFee;
    private StringProperty roomNumber;
    private StringProperty surgeOnFee;
    private StringProperty units;
    private StringProperty unitCost;  
    private StringProperty medication;
    private StringProperty total;

    public BillReport(final int patientID, String lastName, String firstName, String dailyRate, String supplies, String ssupplies, String services, String roomFee, String roomNumber, String surgeOnFee, String units,String unitCost,String medication,String total) {
        this.patientID = new SimpleIntegerProperty(patientID);
        this.lastName = new SimpleStringProperty(lastName);
        this.firstName = new SimpleStringProperty(firstName);
        this.dailyRate = new SimpleStringProperty(dailyRate);
        this.supplies = new SimpleStringProperty(supplies);
        this.ssupplies = new SimpleStringProperty(ssupplies);
        this.services = new SimpleStringProperty(services);
        this.roomFee = new SimpleStringProperty(roomFee);
        this.roomNumber = new SimpleStringProperty(roomNumber);
        this.surgeOnFee = new SimpleStringProperty(surgeOnFee);
        this.units = new SimpleStringProperty(units);
        this.unitCost= new SimpleStringProperty(unitCost);
        this.medication=new SimpleStringProperty(medication);
        this.total = new SimpleStringProperty(total);
        
    }

  public BillReport(){
      this(-1,"","","","","","","","","","","","","");
  }
 

    public int getPatientID() {
        return patientID.get();
    }

    public void setPatientID(final int patientID) {
        this.patientID.set(patientID);
    }
     public IntegerProperty idProperty() {
        return patientID;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(final String lastName) {
        this.lastName.set(lastName);
    }
    public StringProperty lastNameProperty(){
        return lastName;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(final String firstName) {
        this.firstName.set(firstName);
    }
    
        public StringProperty firstNameProperty(){
        return firstName;
    }

    public String getDailyRate() {
        return dailyRate.get();
    }

    public void setDailyRate(final String dailyRate) {
        this.dailyRate.set(dailyRate);
    }
     public StringProperty DailyRateProperty() {
        return dailyRate;
    }

    public String getSupplies() {
        return supplies.get();
    }

    public void setSupplies(final String supplies) {
       this.supplies.set(supplies);
   }

    public StringProperty SuppliesProperty() {
        return supplies;
   }
    
         public String getSsupplies() {
        return supplies.get();
    }

    public void setSsupplies(final String ssupplies) {
        this.ssupplies.set(ssupplies);
   }

    public StringProperty SsuppliesProperty() {
        return ssupplies;
    }
     
     public String getServices(){
         return services.get();
     }

    public void setServices(final String services) {
        this.services.set(services);
    }
      public StringProperty ServicesProperty() {
        return services;
   }
    

    public String getRoomFee() {
        return roomFee.get();
    }

    public void setRoomFee(final String roomFee) {
        this.roomFee.set(roomFee);
    }
    
    public StringProperty RoomFeeProperty(){
        return roomFee;
    }

    public String getRoomNumber() {
        return roomNumber.get();
    }

    public void setRoomNumber(final String roomNumber) {
        this.roomNumber.set(roomNumber);
    }
    public StringProperty roomNumberProperty(){
        return roomNumber;
    }

    public String getSurgeOnFee() {
        return surgeOnFee.get();
    }

    public void setSurgeOnFee(final String surgeOnFee) {
        this.surgeOnFee.set(surgeOnFee);
    }
    public StringProperty surgeOnFeeProperty(){
        return surgeOnFee;
    }

    public String getUnits() {
        return units.get();
    }

    public void setUnits(final String units) {
        this.units.set(units);
    }
    public StringProperty unitsProperty(){
        return units;
    }

    public String  getUnitCost() {
        return unitCost.get();
    }

    public void setUnitCost(final String unitCost) {
        this.unitCost.set(unitCost);
    }
    public StringProperty unitsCostProperty(){
        return unitCost;
    }

    public String getMedication() {
        return medication.get();
    }

    public void setMedication(final String medication) {
        this.medication.set(medication);
    }
    public StringProperty medicationProperty(){
        return medication;
    }
     public String getTotal(){
      return total.get();
  }
  public void setTotal(final String total){
      this.total.set(total);
  }
  public StringProperty totalProperty(){
      return total;
  }



    
    

    @Override
    public int hashCode() {
        int hash = 7;
        
        hash = 53 * hash + Objects.hashCode(this.lastName);
        hash = 53 * hash + Objects.hashCode(this.firstName);
        hash = 53 * hash + Objects.hashCode(this.dailyRate);
        hash = 53 * hash + Objects.hashCode(this.supplies);
        hash = 53 * hash + Objects.hashCode(this.ssupplies);
        hash = 53 * hash + Objects.hashCode(this.services);
        hash = 53 * hash + Objects.hashCode(this.roomFee);
        hash = 53 * hash + Objects.hashCode(this.surgeOnFee);
        hash = 53 * hash + Objects.hashCode(this.units);
        hash = 53 * hash + Objects.hashCode(this.unitCost);
        hash = 53 * hash + Objects.hashCode(this.roomNumber);
        hash = 53 * hash + Objects.hashCode(this.medication);
        
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BillReport other = (BillReport) obj;
        if (this.patientID != other.patientID) {
            return false;
        }
        if (!Objects.equals(this.lastName.get(), other.lastName.get())) {
            return false;
        }
        if (!Objects.equals(this.firstName.get(), other.firstName.get())) {
            return false;
        }
        if (!Objects.equals(this.dailyRate.get(), other.dailyRate.get())) {
            return false;
        }
        if (!Objects.equals(this.supplies.get(), other.supplies.get())) {
            return false;
        }
         if (!Objects.equals(this.ssupplies.get(), other.ssupplies.get())) {
            return false;
        }
        if (!Objects.equals(this.services.get(), other.services.get())) {
            return false;
        }
        if (!Objects.equals(this.roomFee.get(), other.roomFee.get())) {
            return false;
        }
        if (!Objects.equals(this.surgeOnFee.get(), other.surgeOnFee.get())) {
            return false;
        }
        if (!Objects.equals(this.units.get(), other.units.get())) {
            return false;
        }
        if (!Objects.equals(this.unitCost.get(), other.unitCost.get())) {
            return false;
        }
          if (!Objects.equals(this.roomNumber.get(), other.roomNumber.get())) {
            return false;
        }
          if (!Objects.equals(this.medication.get(), other.medication.get())) {
            return false;
        }
          if (!Objects.equals(this.medication.get(), other.medication.get())) {
            return false;
        }
     
        return true;
    }
  
    

    
}
