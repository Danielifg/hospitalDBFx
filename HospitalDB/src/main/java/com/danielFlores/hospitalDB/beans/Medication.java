
package com.danielFlores.hospitalDB.beans;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Daniel
 * 
 * POJO class to store the medical expenses.
 */
public class Medication  {
    private IntegerProperty iD;
    private IntegerProperty patientiD;
    private StringProperty dateOfMed;
    private StringProperty med;
    private DoubleProperty unitCost;
    private DoubleProperty medUnits;
    
    
    public Medication(final int iD,final int patientiD, final String dateOfMed, final String med, final double unitCost, final double medUnits) {
        this.iD = new SimpleIntegerProperty(iD);
        this.patientiD = new SimpleIntegerProperty(patientiD);
        this.dateOfMed = new SimpleStringProperty(dateOfMed);
        this.med = new SimpleStringProperty(med);
        this.unitCost = new SimpleDoubleProperty(unitCost);
        this.medUnits = new SimpleDoubleProperty(medUnits);
    }

    public Medication() {
        this(-1,1,"","",0.0,0.0);
    }
  public int getiD(){
      return iD.get();
  }
      public void setiD(final int iD) {
        this.iD.set(iD);
    }

    public IntegerProperty iD() {
        return iD;
    }
  public int getPatientiD(){
      return patientiD.get();
  }
      public void setPatientiD(final int patientiD) {
        this.patientiD.set(patientiD);
    }

    public IntegerProperty patientiD() {
        return patientiD;
    }

    public String getDateOfMed() {
        return dateOfMed.get();
    }

    public void setDateOfMed(final String dateOfMed) {
        this.dateOfMed.set(dateOfMed);
    }
    
      public StringProperty dateOfMedProperty() {
        return dateOfMed;
    }

        
      public String getMed() {
        return med.get();
    }

    public void setMed(final String roomNumber) {
        this.med.set(roomNumber);
    }

    public StringProperty medProperty() {
        return med;
    }
     public double getUnitCost() {
        return unitCost.get();
    }

    public void setUnitCost(final double unitCost) {
        this.unitCost.set(unitCost);
    }

    public DoubleProperty unitCostProperty() {
        return unitCost;
    }
     public double getMedUnits() {
        return medUnits.get();
    }

    public void setMedUnits(final double medUnits) {
        this.medUnits.set(medUnits);
    }

    public DoubleProperty medUnitsProperty() {
        return medUnits;
    }

    @Override
    public String toString() {
        return "Medication{" + "iD=" + iD + ", patientiD=" + patientiD + ", dateOfMed=" + dateOfMed + ", med=" + med + ", unitCost=" + unitCost + ", medUnits=" + medUnits + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.iD);
        hash = 53 * hash + Objects.hashCode(this.patientiD);
        hash = 53 * hash + Objects.hashCode(this.dateOfMed);
        hash = 53 * hash + Objects.hashCode(this.med);
        hash = 53 * hash + Objects.hashCode(this.unitCost);
        hash = 53 * hash + Objects.hashCode(this.medUnits);
        return hash;
    }


    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Medication other = (Medication) obj;
          if (!Objects.equals(this.iD.get(), other.iD.get())) {
            return false;
        }
        if (!Objects.equals(this.patientiD.get(), other.patientiD.get())) {
            return false;
        }
        if (!Objects.equals(this.dateOfMed.get(), other.dateOfMed.get())) {
            return false;
        }
        if (!Objects.equals(this.med.get(), other.med.get())) {
            return false;
        }
        if (!Objects.equals(this.unitCost.get(), other.unitCost.get())) {
            return false;
        }
        if (!Objects.equals(this.medUnits.get(), other.medUnits.get())) {
            return false;
        }
        return true;
    }
    
    
}
