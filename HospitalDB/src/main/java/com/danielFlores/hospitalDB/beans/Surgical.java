
package com.danielFlores.hospitalDB.beans;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Daniel
 * 
 * POJO for data of patients that required surgery.
 * 
 */
public class Surgical {
    private IntegerProperty id;
    private IntegerProperty patient_id;
    private  StringProperty dateOfSurgery;
    private StringProperty surgery;
    private DoubleProperty roomFee;
    private DoubleProperty surgeOnFee;
    private DoubleProperty supplies;

    public Surgical() {
        this(-1,-1, "", "", 0.0, 0.0, 0.0);

    }

    public Surgical(final int id,final int patient_id, final String dateOfSurgery,final String surgery, final double roomFee,final double surgeOnFee,final double supplies) {
        this.id =new SimpleIntegerProperty(id);
        this.patient_id = new SimpleIntegerProperty(patient_id);
        this.dateOfSurgery = new SimpleStringProperty(dateOfSurgery);
        this.surgery = new SimpleStringProperty(surgery);
        this.roomFee = new SimpleDoubleProperty(roomFee);
        this.surgeOnFee = new SimpleDoubleProperty(surgeOnFee);
        this.supplies = new SimpleDoubleProperty(supplies);
    }
    public int getid(){
      return id.get();
  }
      public void setid(final int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public int getPatient_id(){
      return patient_id.get();
  }
      public void setPatient_id(final int patient_id) {
        this.patient_id.set(patient_id);
    }

   public IntegerProperty patient_idProperty() {
       return patient_id;
    }

    public String getDateOfSurgery() {
        return dateOfSurgery.get();
    }

    public void setDateOfSurgery(String dateOfSurgery) {
        this.dateOfSurgery.set(dateOfSurgery);
    }
       public StringProperty surgeryDateOFProperty() {
        return dateOfSurgery;
    }
    
    
      public String getSurgery() {
        return surgery.get();
    }

    public void setSurgery(final String surgery) {
        this.surgery.set(surgery);
    }

    public StringProperty surgeryProperty() {
        return surgery;
    }
     public double getRoomFee() {
        return roomFee.get();
    }

    public void setRoomFee(final double roomFee) {
        this.roomFee.set(roomFee);
    }

    public DoubleProperty roomFeeProperty() {
        return roomFee;
    }
     public double getSurgeOnFee() {
        return surgeOnFee.get();
    }

    public void setSurgeOnFee(final double surgeOnFee) {
        this.surgeOnFee.set(surgeOnFee);
    }

    public DoubleProperty surgeOnFeeProperty() {
        return surgeOnFee;
    }
    
     public double getSupplies() {
        return supplies.get();
    }

    public void setSupplies(final double supplies) {
        this.supplies.set(supplies);
    }

    public DoubleProperty supppliesProperty() {
        return supplies;
    }

    @Override
    public String toString() {
        return "Surgical{" + "patient_id=" + patient_id + ", dateOfSurgery=" + dateOfSurgery + ", surgery=" + surgery + ", roomFee=" + roomFee + ", surgeOnFee=" + surgeOnFee + ", supplies=" + supplies + '}';
    }
    
          @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((surgery.get() == null) ? 0 : surgery.get().hashCode());
      
        return result;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final Surgical other = (Surgical) obj;
//        if (!Objects.equals(this.surgery, other.surgery)) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Surgical other = (Surgical) obj;
        if (!Objects.equals(this.id.get(), other.id.get())) {
            return false;
        }
        if (!Objects.equals(this.patient_id.get(), other.patient_id.get())) {
            return false;
        }
        if (!Objects.equals(this.dateOfSurgery.get(), other.dateOfSurgery.get())) {
            return false;
        }
        if (!Objects.equals(this.surgery.get(), other.surgery.get())) {
            return false;
        }
        if (!Objects.equals(this.roomFee.get(), other.roomFee.get())) {
            return false;
        }
        if (!Objects.equals(this.surgeOnFee.get(), other.surgeOnFee.get())) {
            return false;
        }
        if (!Objects.equals(this.supplies.get(), other.supplies.get())) {
            return false;
        }
        return true;
    }

    
    
}
    

    