
package com.danielFlores.hospitalDB.beans;

import java.util.List;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Daniel
 * 
 * Main Patient information in the first scene.
 * 
 */
public class Patient {

    private IntegerProperty Id;
    private StringProperty lastName;
    private StringProperty firstName;
    private StringProperty diagnosis;
    private StringProperty admissionDate;
    private StringProperty releaseDate; // static string timestamp value of String .valueOf("String");

    private ObservableList<Inpatient> listOfInpatients;
    private ObservableList<Medication> listOfMedications;
    private ObservableList<Surgical> listOfSurgical;
    private ObservableList<BillReport> bill;

    public Patient(final int Id, final String lastName, final String firstName, final String diagnosis, final String admissionDate, final String releaseDate) {

        super();
        this.Id = new SimpleIntegerProperty(Id);
        this.lastName = new SimpleStringProperty(lastName);
        this.firstName = new SimpleStringProperty(firstName);
        this.diagnosis = new SimpleStringProperty(diagnosis);
        this.admissionDate = new SimpleStringProperty(admissionDate);
        this.releaseDate = new SimpleStringProperty(releaseDate);

    }

    public Patient() {
        this(-1, "", "", "", "", "");
    }

    // **************** Lists *******************************//
    public ObservableList<Inpatient> getListOfInpatients() {
        return listOfInpatients;
    }

    public void setListOfInpatients(ObservableList<Inpatient> listOfInpatients) {
        this.listOfInpatients = listOfInpatients;
    }

    public ObservableList<Medication> getListOfMedications() {
        return listOfMedications;
    }

    public void setListOfMedications(ObservableList<Medication> listOfMedications) {
        this.listOfMedications = listOfMedications;
    }

    public ObservableList<Surgical> getListOfSurgical() {
        return listOfSurgical;
    }

    public void setListOfSurgical(ObservableList<Surgical> listOfSurgical) {
        this.listOfSurgical = listOfSurgical;
    }

    public List<BillReport> getBill() {
        return bill;
    }

    public void setBill(ObservableList<BillReport> bill) {
        this.bill = bill;
    }

//**********************************************************//
    public int getId() {
        return Id.get();
    }

    public void setId(final int id) {
        this.Id.set(id);
    }

    public IntegerProperty idProperty() {
        return Id;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(final String lastName) {
        this.lastName.set(lastName);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(final String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public String getDiagnosis() {
        return diagnosis.get();
    }

    public void setDiagnosis(final String diagnosis) {
        this.diagnosis.set(diagnosis);
    }

    public StringProperty diagnosisProperty() {
        return diagnosis;
    }

    public String getAdmissionDate() {
        return admissionDate.get();
    }

    public void setAdmissionDate(final String releaseDate) {
        this.admissionDate.set(releaseDate);
    }

    public StringProperty admissionDateProperty() {
        return admissionDate;

    }

    public String getReleaseDate() {
        return releaseDate.get();
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate.set(releaseDate);
    }

    public StringProperty releaseDateProperty() {
        return releaseDate;
    }

    @Override
    public String toString() {
        return "Patient{" + "Id=" + Id + ", lastName=" + lastName + ", firstName=" + firstName + ", diagnosis=" + diagnosis + ", admissionDate=" + admissionDate + ", releaseDate=" + releaseDate + ", listOfInpatients=" + listOfInpatients + ", listOfMedications=" + listOfMedications + ", listOfSurgical=" + listOfSurgical + '}';
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((lastName.get() == null) ? 0 : lastName.get().hashCode());
        result = prime * result
                + ((firstName.get() == null) ? 0 : firstName.get().hashCode());
        result = prime * result
                + ((diagnosis.get() == null) ? 0 : diagnosis.get().hashCode());
        result = prime * result
                + ((admissionDate.get() == null) ? 0 : admissionDate.get().hashCode());
        result = prime * result + ((releaseDate.get() == null) ? 0 : releaseDate.get().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Patient other = (Patient) obj;

        if (!Objects.equals(this.lastName.get(), other.lastName.get())) {
            return false;
        }
        if (!Objects.equals(this.firstName.get(), other.firstName.get())) {
            return false;
        }
        if (!Objects.equals(this.diagnosis.get(), other.diagnosis.get())) {
            return false;
        }
        if (!Objects.equals(this.admissionDate.get(), other.admissionDate.get())) {
            return false;
        }
        if (!Objects.equals(this.releaseDate.get(), other.releaseDate.get())) {
            return false;
        }
        if (!Objects.equals(this.listOfInpatients, other.listOfInpatients)) {
            return false;
        }
        if (!Objects.equals(this.listOfMedications, other.listOfMedications)) {
            return false;
        }
        if (!Objects.equals(this.listOfSurgical, other.listOfSurgical)) {
            return false;
        }

        return true;
    }

}
