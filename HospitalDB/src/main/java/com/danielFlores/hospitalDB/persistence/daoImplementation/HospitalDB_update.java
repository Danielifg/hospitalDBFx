
package com.danielFlores.hospitalDB.persistence.daoImplementation;

import com.danielFlores.hospitalDB.beans.Inpatient;
import com.danielFlores.hospitalDB.beans.Medication;
import com.danielFlores.hospitalDB.beans.Patient;
import com.danielFlores.hospitalDB.beans.Surgical;
import com.danielFlores.hospitalDB.persistence.daoDeclaration.HospitalDAO_update;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Daniel
 */
public class HospitalDB_update implements HospitalDAO_update {
   
    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "root";
    private final String password = "";

    @Override
    public int updatePatient(Patient patient) throws SQLException {
        int updateResult;

        String updateQuery = "UPDATE PATIENT SET LASTNAME=?,FIRSTNAME=?,DIAGNOSIS=?,ADMISSIONDATE=?,RELEASEDATE=? WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {

            ps.setString(1, patient.getLastName());
            ps.setString(2, patient.getFirstName());
            ps.setString(3, patient.getDiagnosis());
            ps.setString(4, patient.getAdmissionDate());
            ps.setString(5, patient.getReleaseDate());
            ps.setInt(6, patient.getId());

            updateResult = ps.executeUpdate();
        }

        return updateResult;
    }

    @Override
    public int updateInpatient(Inpatient inpatient) throws SQLException {
        int updateResult;

        String updateQuery = "UPDATE INPATIENT SET DATEOFSTAY=?,ROOMNUMBER=?,DAILYRATE=?,SUPPLIES=?,SERVICES=? WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {

            ps.setString(1, inpatient.getDateOfStay());
            ps.setString(2, inpatient.getRoomNumber());
            ps.setDouble(3, inpatient.getDailyRate());
            ps.setDouble(4, inpatient.getSupplies());
            ps.setDouble(5, inpatient.getServices());
            ps.setInt(6, inpatient.getInpatientID());

            updateResult = ps.executeUpdate();
        }

        return updateResult;
    }

    @Override
    public int updateMedication(Medication medication) throws SQLException {
        int updateResult;

        String updateQuery = "UPDATE MEDICATION SET PATIENTID=?,DATEOFMED=?,MED=?,UNITCOST=?,UNITS=? WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {

            ps.setInt(1, medication.getPatientiD());
            ps.setString(2, medication.getDateOfMed());
            ps.setString(3, medication.getMed());
            ps.setDouble(4, medication.getUnitCost());
            ps.setDouble(5, medication.getMedUnits());
            ps.setDouble(6, medication.getiD());

            updateResult = ps.executeUpdate();
        }

        return updateResult;
    }

    @Override
    public int updateSurgical(Surgical surgical) throws SQLException {
        int updateResult;

        String updateQuery = "UPDATE SURGICAL SET PATIENTID=?,DATEOFSURGERY=?,SURGERY=?,ROOMFEE=?,SURGEONFEE=?,SUPPLIES=? WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {

            ps.setInt(1, surgical.getPatient_id());
            ps.setString(2, surgical.getDateOfSurgery());
            ps.setString(3, surgical.getSurgery());
            ps.setDouble(4, surgical.getRoomFee());
            ps.setDouble(5, surgical.getSurgeOnFee());
            ps.setDouble(6, surgical.getSupplies());
            ps.setDouble(7, surgical.getid());

            updateResult = ps.executeUpdate();
        }

        return updateResult;
    }

}
