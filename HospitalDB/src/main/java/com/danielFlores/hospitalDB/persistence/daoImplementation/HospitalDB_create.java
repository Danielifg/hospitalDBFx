
package com.danielFlores.hospitalDB.persistence.daoImplementation;

import com.danielFlores.hospitalDB.beans.Inpatient;
import com.danielFlores.hospitalDB.beans.Medication;
import com.danielFlores.hospitalDB.beans.Patient;
import com.danielFlores.hospitalDB.beans.Surgical;
import com.danielFlores.hospitalDB.persistence.daoDeclaration.HospitalDAO_create;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Daniel
 * 
 * Create new records in Data Base.
 */
public class HospitalDB_create implements HospitalDAO_create {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "root";
    private final String password = "";

    @Override
    public int createPatient(Patient patient) throws SQLException {
        int result;
        String query = "INSERT INTO PATIENT (LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE) VALUES (?,?,?,?,?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pS = connection.prepareStatement(query);) {

            pS.setString(1, patient.getLastName());
            pS.setString(2, patient.getFirstName());
            pS.setString(3, patient.getDiagnosis());
            pS.setString(4, patient.getAdmissionDate());
            pS.setString(5, patient.getReleaseDate());

            result = pS.executeUpdate();

        }
        log.info("# of records created: " + result);
        return result;
    }

    @Override
    public int createInpatient(Inpatient inpatient) throws SQLException {
        int result;
        String query = "INSERT INTO INPATIENT (PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES) VALUES (?,?,?,?,?,?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pS = connection.prepareStatement(query);) {

            pS.setInt(1, inpatient.getInpatientID());
            pS.setString(2, inpatient.getDateOfStay());
            pS.setString(3, inpatient.getRoomNumber());
            pS.setDouble(4, inpatient.getDailyRate());
            pS.setDouble(5, inpatient.getSupplies());
            pS.setDouble(6, inpatient.getServices());

            result = pS.executeUpdate();

        }
        log.info("# of records created: " + result);
        return result;
    }

    @Override
    public int createMedication(Medication medication) throws SQLException {
        int result;
        String query = "INSERT INTO MEDICATION (PATIENTID,DATEOFMED,MED,UNITCOST,UNITS) VALUES (?,?,?,?,?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pS = connection.prepareStatement(query);) {

            pS.setInt(1, medication.getPatientiD());
            pS.setString(2, medication.getDateOfMed());
            pS.setString(3, medication.getMed());
            pS.setDouble(4, medication.getUnitCost());
            pS.setDouble(5, medication.getMedUnits());

            result = pS.executeUpdate();

        }
        log.info("# of records created: " + result);
        return result;
    }

    @Override
    public int createSurgical(Surgical surgical) throws SQLException {
        int result;
        String query = "INSERT INTO SURGICAL (PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES) VALUES (?,?,?,?,?,?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pS = connection.prepareStatement(query);) {

            pS.setInt(1, surgical.getPatient_id());
            pS.setString(2, surgical.getDateOfSurgery());
            pS.setString(3, surgical.getSurgery());
            pS.setDouble(4, surgical.getRoomFee());
            pS.setDouble(5, surgical.getSurgeOnFee());
            pS.setDouble(6, surgical.getSupplies());

            result = pS.executeUpdate();

        }
        log.info("# of records created: " + result);
        return result;

    }

}
