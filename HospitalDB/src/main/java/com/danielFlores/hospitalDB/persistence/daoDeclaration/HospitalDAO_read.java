
package com.danielFlores.hospitalDB.persistence.daoDeclaration;

import com.danielFlores.hospitalDB.beans.BillReport;
import com.danielFlores.hospitalDB.beans.Inpatient;
import com.danielFlores.hospitalDB.beans.Medication;
import com.danielFlores.hospitalDB.beans.Patient;
import com.danielFlores.hospitalDB.beans.Surgical;
import java.sql.SQLException;
import javafx.collections.ObservableList;

/**
 *
 * @author Daniel
 * 
 * Data Access Object Declaration_READ.
 * 
 */
public interface HospitalDAO_read {

    public Patient findRecordsById(int Id) throws SQLException;

    public Inpatient findInpatientById(int id) throws SQLException;

    public Surgical findSurgicalById(int id) throws SQLException;

    public Medication findMedicationById(int id) throws SQLException;

    public Patient findRecordsByLastName(String lastName) throws SQLException;

    public ObservableList<Inpatient> displayInpatientDB() throws SQLException;

    public ObservableList<Medication> displayMedicationDB() throws SQLException;

    public ObservableList<Surgical> displaySurgicalDB() throws SQLException;

    public ObservableList<Patient> findAllRecords() throws SQLException;

    // ******* BILL - REPORT ************************ //
    public BillReport findBillbyId(int id) throws SQLException;

    public BillReport displayReport(int id) throws SQLException;

    public BillReport displayTotal(int id) throws SQLException;

    // ******* PREVIOUS - NEXT PATIENT ************************ //
    public Patient findNextPatientByID(Patient patient) throws SQLException;

    public Patient findPreviousPatientByID(Patient patient) throws SQLException;

    public Inpatient findNextInpatientByID(Inpatient inpatient) throws SQLException;

    public Inpatient findPreviousInpatientByID(Inpatient inpatient) throws SQLException;

    public Surgical findNextSurgicalByID(Surgical surgical) throws SQLException;

    public Surgical findPreviousSurgicalByID(Surgical surgical) throws SQLException;

    public Medication findNextMedicationByID(Medication medication) throws SQLException;

    public Medication findPreviousMedicationtByID(Medication medication) throws SQLException;

}
