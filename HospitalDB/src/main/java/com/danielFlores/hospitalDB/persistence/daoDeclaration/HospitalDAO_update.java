
package com.danielFlores.hospitalDB.persistence.daoDeclaration;

import com.danielFlores.hospitalDB.beans.Inpatient;
import com.danielFlores.hospitalDB.beans.Medication;
import com.danielFlores.hospitalDB.beans.Patient;
import com.danielFlores.hospitalDB.beans.Surgical;
import java.sql.SQLException;

/**
 *
 * @author Daniel
 * 
 * Data Access Object Declaration_UPDATE.
 */
public interface HospitalDAO_update {

    public int updatePatient(Patient patient) throws SQLException;

    public int updateInpatient(Inpatient inpatient) throws SQLException;

    public int updateMedication(Medication medication) throws SQLException;

    public int updateSurgical(Surgical surgical) throws SQLException;

}
