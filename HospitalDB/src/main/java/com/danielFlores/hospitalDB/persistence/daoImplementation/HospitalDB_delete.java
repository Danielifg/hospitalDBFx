/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielFlores.hospitalDB.persistence.daoImplementation;

import com.danielFlores.hospitalDB.persistence.daoDeclaration.HospitalDAO_delete;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Daniel
 * 
 * Delete Methods implementations for results of ID queries.
 * 
 */
public class HospitalDB_delete implements HospitalDAO_delete {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "root";
    private final String password = "";

    @Override
    public int deleteInpatient(int inpatient) throws SQLException {
        int result;

        String deleteQuery = "DELETE FROM INPATIENT WHERE PATIENTID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, inpatient);
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;

    }

    @Override
    public int deleteMedication(int medication) throws SQLException {
        int result;

        String deleteQuery = "DELETE FROM MEDICATION WHERE PATIENTID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, medication);
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    @Override
    public int deleteSurgical(int surgical) throws SQLException {
        int result;

        String deleteQuery = "DELETE FROM SURGICAL WHERE PATIENTID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, surgical);
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    @Override
    public int deleteMaster(int master) throws SQLException {
        int result;

        String deleteQuery = "DELETE FROM PATIENT WHERE PATIENTID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, master);
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

}
