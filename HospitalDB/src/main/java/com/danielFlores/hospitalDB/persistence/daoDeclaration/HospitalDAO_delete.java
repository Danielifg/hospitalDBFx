
package com.danielFlores.hospitalDB.persistence.daoDeclaration;

import java.sql.SQLException;

/**
 *
 * @author Daniel
 * 
 * Data Access Object Declaration_DELETE.
 */
public interface HospitalDAO_delete {

    public int deleteInpatient(int inpatient) throws SQLException;

    public int deleteMedication(int medication) throws SQLException;

    public int deleteSurgical(int surgical) throws SQLException;

    public int deleteMaster(int master) throws SQLException;
}
