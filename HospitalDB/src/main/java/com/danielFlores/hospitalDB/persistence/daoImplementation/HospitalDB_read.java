package com.danielFlores.hospitalDB.persistence.daoImplementation;

import com.danielFlores.hospitalDB.beans.Inpatient;
import com.danielFlores.hospitalDB.beans.Medication;
import com.danielFlores.hospitalDB.beans.Patient;
import com.danielFlores.hospitalDB.beans.Surgical;
import com.danielFlores.hospitalDB.beans.BillReport;
import com.danielFlores.hospitalDB.persistence.daoDeclaration.HospitalDAO_read;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Daniel
 *
 * Data Access Object CRUD implementation.
 *
 */
public class HospitalDB_read implements HospitalDAO_read {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "root";
    private final String password = "";

    public HospitalDB_read() {
        super();
    }

    // *********************  RETRIVE  ********************************//
    @Override
    public Patient findRecordsById(int id) throws SQLException {
        Patient patient = null;
        String query = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    patient = createPatientData(resultSet);
                }
            }
        }

        log.info("Found" + id + "?: " + (patient != null));
        return patient;
    }

    @Override
    public Inpatient findInpatientById(int id) throws SQLException {
        Inpatient inpatient = null;
        String query = "SELECT PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    inpatient = createInpatientData(resultSet);
                }
            }
        }

        log.info("Found" + id + "?: " + (inpatient != null));
        return inpatient;
    }

    @Override
    public Surgical findSurgicalById(int id) throws SQLException {
        Surgical surgical = null;
        String query = "SELECT PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    surgical = createSurgicalData(resultSet);
                }
            }
        }

        log.info("Found" + id + "?: " + (surgical != null));
        return surgical;
    }

    @Override
    public Medication findMedicationById(int id) throws SQLException {
        Medication medication = null;
        String query = "SELECT PATIENTID,DATEOFMED,MED,UNITCOST,UNITS FROM MEDICATION WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    medication = createMedicationData(resultSet);
                }
            }
        }

        log.info("Found" + id + "?: " + (medication != null));
        return medication;
    }

    @Override
    public BillReport findBillbyId(int id) throws SQLException {
        BillReport bill = null;

        String query = "SELECT DISTINCT P.PATIENTID,P.LASTNAME,P.FIRSTNAME, I.ROOMNUMBER,SUM(I.DAILYRATE) AS DAILYRATE,CONCAT(SUM(I.SUPPLIES)+SUM(S.SUPPLIES)) AS SUPPLIES,SUM(I.SERVICES) AS SERVICES,SUM(S.ROOMFEE) AS ROOMFEE, SUM(S.SURGEONFEE) AS SURGEONFEE ,M.UNITCOST,M.UNITS, CONCAT ('$ ',FORMAT((UNITS)*(UNITCOST),2))AS MEDICATION FROM PATIENT P, INPATIENT I, SURGICAL S, MEDICATION M WHERE P.PATIENTID = I.ID   AND S.ID = M.ID AND  P.PATIENTID = ?;";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    bill = createBillReportData(resultSet);
                }
            }
        }
        return bill;
    }

    private Patient createPatientData(ResultSet resultSet) throws SQLException {
        Patient patientData = new Patient();

        patientData.setId(resultSet.getInt("PATIENTID"));
        patientData.setLastName(resultSet.getString("LASTNAME"));
        patientData.setFirstName(resultSet.getString("FIRSTNAME"));
        patientData.setDiagnosis(resultSet.getString("DIAGNOSIS"));
        patientData.setAdmissionDate(resultSet.getString("ADMISSIONDATE"));
        patientData.setReleaseDate(resultSet.getString("RELEASEDATE"));

        return patientData;
    }

    private Inpatient createInpatientData(ResultSet rSet) throws SQLException {
        Inpatient inpatient = new Inpatient();

        inpatient.setInpatientID(rSet.getInt("PATIENTID"));
        inpatient.setDateOfStay(rSet.getString("DATEOFSTAY"));
        inpatient.setRoomNumber(rSet.getString("ROOMNUMBER"));
        inpatient.setdailyRate(rSet.getDouble("DAILYRATE"));
        inpatient.setSupplies(rSet.getDouble("SUPPLIES"));
        inpatient.setServices(rSet.getDouble("SERVICES"));
        return inpatient;
    }

    private Surgical createSurgicalData(ResultSet rSet) throws SQLException {
        Surgical surgical = new Surgical();
        surgical.setPatient_id(rSet.getInt("PATIENTID"));
        surgical.setDateOfSurgery(rSet.getString("DATEOFSURGERY"));
        surgical.setSurgery(rSet.getString("SURGERY"));
        surgical.setRoomFee(rSet.getDouble("ROOMFEE"));
        surgical.setSurgeOnFee(rSet.getDouble("SURGEONFEE"));
        surgical.setSupplies(rSet.getDouble("SUPPLIES"));
        return surgical;
    }

    private Medication createMedicationData(ResultSet rSet) throws SQLException {
        Medication medication = new Medication();

        medication.setPatientiD(rSet.getInt("PATIENTID"));
        medication.setDateOfMed(rSet.getString("DATEOFMED"));
        medication.setMed(rSet.getString("MED"));
        medication.setUnitCost(rSet.getDouble("UNITCOST"));
        medication.setMedUnits(rSet.getDouble("UNITS"));
        return medication;
    }

    @Override
    public Patient findRecordsByLastName(String lastName) throws SQLException {
        Patient patient = null;
        String query = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT WHERE LASTNAME = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setString(1, lastName);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    patient = createPatientData(resultSet);
                }
            }
        }

        log.info("Found" + lastName + "?: " + (patient != null));
        return patient;
    }

    @Override
    public ObservableList<Patient> findAllRecords() throws SQLException {

        ObservableList<Patient> hospitalDBList = FXCollections.observableArrayList();

        String findAllPatient = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preStatement = connection.prepareStatement(findAllPatient);
                ResultSet rSet = preStatement.executeQuery()) {

            while (rSet.next()) {
                Patient patient = new Patient();
                patient.setId(rSet.getInt("PATIENTID"));
                patient.setLastName(rSet.getString("LASTNAME"));
                patient.setFirstName(rSet.getString("FIRSTNAME"));
                patient.setDiagnosis(rSet.getString("DIAGNOSIS"));
                patient.setAdmissionDate(rSet.getString("ADMISSIONDATE"));
                patient.setReleaseDate(rSet.getString("RELEASEDATE"));

                // findInpatient(patient);
                // findAllSurgicalData(patient);                
                //findAllMedicationData(patient);
                hospitalDBList.add(patient);

            }
        }
        log.info("The number of records =" + hospitalDBList.size());
        return hospitalDBList;
    }

    @Override
    public ObservableList<Inpatient> displayInpatientDB() throws SQLException {

        ObservableList<Inpatient> InpatientDBList = FXCollections.observableArrayList();

        String findAllInpatient = "SELECT PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preStatement = connection.prepareStatement(findAllInpatient);
                ResultSet rSet = preStatement.executeQuery()) {

            while (rSet.next()) {
                Inpatient inpatient = new Inpatient();
                inpatient.setInpatientID(rSet.getInt("PATIENTID"));
                inpatient.setDateOfStay(rSet.getString("DATEOFSTAY"));
                inpatient.setRoomNumber(rSet.getString("ROOMNUMBER"));
                inpatient.setdailyRate(rSet.getDouble("DAILYRATE"));
                inpatient.setSupplies(rSet.getDouble("SUPPLIES"));
                inpatient.setServices(rSet.getDouble("SERVICES"));
                InpatientDBList.add(inpatient);
            }

            return InpatientDBList;
        }
    }

    @Override
    public ObservableList<Surgical> displaySurgicalDB() throws SQLException {

        ObservableList<Surgical> SurgicalDBList = FXCollections.observableArrayList();
        String findAllSurgical = "SELECT PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preStatement = connection.prepareStatement(findAllSurgical);
                ResultSet rSet = preStatement.executeQuery()) {

            while (rSet.next()) {
                Surgical surgical = new Surgical();
                surgical.setPatient_id(rSet.getInt("PATIENTID"));
                surgical.setDateOfSurgery(rSet.getString("DATEOFSURGERY"));
                surgical.setSurgery(rSet.getString("SURGERY"));
                surgical.setRoomFee(rSet.getDouble("ROOMFEE"));
                surgical.setSurgeOnFee(rSet.getDouble("SURGEONFEE"));
                surgical.setSupplies(rSet.getDouble("SUPPLIES"));
                SurgicalDBList.add(surgical);
            }
            log.info("Surgical List" + SurgicalDBList.size());
            return SurgicalDBList;
        }
    }

    @Override
    public ObservableList<Medication> displayMedicationDB() throws SQLException {
        ObservableList<Medication> MedicationDBList = FXCollections.observableArrayList();

        String findAllMedication = "SELECT PATIENTID,DATEOFMED,MED,UNITCOST,UNITS FROM MEDICATION";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preStatement = connection.prepareStatement(findAllMedication);
                ResultSet rSet = preStatement.executeQuery()) {

            while (rSet.next()) {
                Medication medication = new Medication();
                medication.setPatientiD(rSet.getInt("PATIENTID"));
                medication.setDateOfMed(rSet.getString("DATEOFMED"));
                medication.setMed(rSet.getString("MED"));
                medication.setUnitCost(rSet.getDouble("UNITCOST"));
                medication.setMedUnits(rSet.getDouble("UNITS"));
                MedicationDBList.add(medication);
            }
//            log.info(MedicationDBList.size());

            return MedicationDBList;
        }
    }

    public BillReport displayReport(int id) throws SQLException {
        BillReport bill = null;

        String queryAll = "SELECT DISTINCT P.PATIENTID,P.LASTNAME,P.FIRSTNAME, I.ROOMNUMBER,\n"
                + "                CONCAT('$ ',FORMAT(SUM(I.DAILYRATE),2)) AS DAILYRATE,\n"
                + "                CONCAT('$ ',FORMAT(SUM(I.SUPPLIES)+(S.SUPPLIES),2)) AS SUPPLIES,\n"
                + "                CONCAT('$ ',FORMAT(SUM(I.SERVICES),2)) AS SERVICES,\n"
                + "                CONCAT('$ ',S.ROOMFEE) AS ROOMFEE,\n"
                + "                CONCAT('$ ',S.SURGEONFEE) AS SURGEONFEE ,\n"
                + "                CONCAT('$ ',FORMAT((M.UNITS)*(M.UNITCOST),2))AS MEDICATION\n"
                + "                       FROM PATIENT P, INPATIENT I, SURGICAL S, MEDICATION M WHERE P.PATIENTID = I.ID \n"
                + "                      AND S.ID = M.ID AND  P.PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preStatement = connection.prepareStatement(queryAll);) {
            preStatement.setInt(1, id);
            try (ResultSet rSet = preStatement.executeQuery()) {
                if (rSet.next()) {
                    bill = createBillReportData(rSet);

                }

                return bill;

            }

        }
    }

    public BillReport displayTotal(int id) throws SQLException {
        BillReport bill = new BillReport();

        String queryAll = "SELECT CONCAT('$ ',FORMAT(SUM(I.DAILYRATE)+SUM(I.SUPPLIES)+S.SUPPLIES+SUM(I.SERVICES)+S.ROOMFEE+S.SURGEONFEE+((M.UNITS)*(M.UNITCOST)),2)) AS TOTAL\n"
                + "                       FROM PATIENT P, INPATIENT I, SURGICAL S, MEDICATION M WHERE P.PATIENTID = I.ID \n"
                + "                      AND S.ID = M.ID AND  P.PATIENTID = ?;";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preStatement = connection.prepareStatement(queryAll);) {
            preStatement.setInt(1, id);
            try (ResultSet rSet = preStatement.executeQuery()) {
                if (rSet.next()) {
                    bill.setTotal(rSet.getString("TOTAL"));

                }

                return bill;

            }

        }
    }

    public void findInpatient(Patient patient) throws SQLException {

        String findAllInpatient = "SELECT ID, PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preStatement = connection.prepareStatement(findAllInpatient)) {
            preStatement.setInt(1, patient.getId());

            try (ResultSet rSet = preStatement.executeQuery()) {
                while (rSet.next()) {

                    Inpatient inpatient = new Inpatient();

                    inpatient.setInpatientID(rSet.getInt("PATIENTID"));
                    inpatient.setDateOfStay(rSet.getString("DATEOFSTAY"));
                    inpatient.setRoomNumber(rSet.getString("ROOMNUMBER"));// rummnumber i not a int
                    inpatient.setdailyRate(rSet.getDouble("DAILYRATE"));
                    inpatient.setSupplies(rSet.getDouble("SUPPLIES"));
                    inpatient.setServices(rSet.getDouble("SERVICES"));
                    inpatient.setServices(rSet.getDouble("ID"));

                    patient.getListOfInpatients().add(inpatient);
                }
            }
        }

    }

    public void findAllSurgicalData(Patient patient) throws SQLException {
        String findAllSurgicalData = "SELECT PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preStatement = connection.prepareStatement(findAllSurgicalData);
                ResultSet rSet = preStatement.executeQuery(findAllSurgicalData)) {

            while (rSet.next()) {
                Surgical surgical = new Surgical();

                surgical.setPatient_id(rSet.getInt("PATIENTID"));
                surgical.setDateOfSurgery(rSet.getString("DATEOFSURGERY"));
                surgical.setSurgery(rSet.getString("SURGERY"));
                surgical.setRoomFee(rSet.getDouble("ROOMFEE"));
                surgical.setSurgeOnFee(rSet.getDouble("SURGEONFEE"));
                surgical.setSupplies(rSet.getDouble("SUPPLIES"));

                patient.getListOfSurgical().add(surgical);
            }
        }
    }

    public void findAllMedicationData(Patient patient) throws SQLException {

        String findAllMedicationData = "SELECT PATIENTID,DATEOFMED,MED,UNICOST,UNITS FROM MEDICATION";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preStatement = connection.prepareStatement(findAllMedicationData);
                ResultSet rSet = preStatement.executeQuery(findAllMedicationData)) {

            while (rSet.next()) {
                Medication medication = new Medication();
                medication.setPatientiD(rSet.getInt("PATIENTID"));
                medication.setDateOfMed(rSet.getString("DATEOFMED"));
                medication.setMed(rSet.getString("MED"));
                medication.setUnitCost(rSet.getDouble("UNITCOST"));
                medication.setMedUnits(rSet.getDouble("UNITS"));

                patient.getListOfMedications().add(medication);
            }
        }
    }

    //************** FIND PREVIOUS-NEXT AND BOUND ************************************//
    @Override
    public Patient findNextPatientByID(Patient patient) throws SQLException {
        String queryNext = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT WHERE PATIENTID = (SELECT MIN(PATIENTID) FROM PATIENT WHERE PATIENTID > ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(queryNext);) {
            pStatement.setInt(1, patient.getId());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundPatientData(resultSet, patient);
                }
            }
        }
        log.info("Found " + patient.getId() + "???: " + (patient));
        return patient;
    }

    @Override
    public Patient findPreviousPatientByID(Patient patient) throws SQLException {

        String queryPrevious = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE,RELEASEDATE FROM PATIENT WHERE PATIENTID = (SELECT MAX(PATIENTID) FROM PATIENT WHERE PATIENTID < ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(queryPrevious);) {
            pStatement.setInt(1, patient.getId());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundPatientData(resultSet, patient);
                }
            }
        }
        log.info("FoundddDD " + patient.getId() + "???: " + (patient != null));
        return patient;
    }

    @Override
    public Inpatient findNextInpatientByID(Inpatient inpatient) throws SQLException {
        String queryNext = "SELECT PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT WHERE PATIENTID = (SELECT MIN(PATIENTID) FROM INPATIENT WHERE PATIENTID > ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(queryNext);) {
            pStatement.setInt(1, inpatient.getInpatientID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundInpatientData(resultSet, inpatient);
                }
            }
        }
        log.info("Found " + inpatient.getInpatientID() + "???: " + (inpatient != null));
        return inpatient;
    }

    @Override
    public Inpatient findPreviousInpatientByID(Inpatient inpatient) throws SQLException {
        String queryNext = "SELECT PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES,SERVICES FROM INPATIENT WHERE PATIENTID = (SELECT MAX(PATIENTID) FROM INPATIENT WHERE PATIENTID < ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(queryNext);) {
            pStatement.setInt(1, inpatient.getInpatientID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundInpatientData(resultSet, inpatient);
                }
            }
        }
        log.info("Found " + inpatient.getInpatientID() + "???: " + (inpatient != null));
        return inpatient;
    }

    @Override
    public Surgical findNextSurgicalByID(Surgical surgical) throws SQLException {

        String queryNext = "SELECT PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL WHERE PATIENTID = (SELECT MIN(PATIENTID) FROM SURGICAL WHERE PATIENTID > ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(queryNext);) {
            pStatement.setInt(1, surgical.getPatient_id());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundSurgicalData(resultSet, surgical);
                }
            }
        }
        log.info("Found " + surgical.getPatient_id() + "???: " + (surgical != null));
        return surgical;
    }

    @Override
    public Surgical findPreviousSurgicalByID(Surgical surgical) throws SQLException {
        String queryNext = "SELECT PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES FROM SURGICAL WHERE PATIENTID = (SELECT MAX(PATIENTID) FROM SURGICAL WHERE PATIENTID < ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(queryNext);) {
            pStatement.setInt(1, surgical.getPatient_id());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundSurgicalData(resultSet, surgical);
                }
            }
        }
        log.info("Found " + surgical.getPatient_id() + "???: " + (surgical != null));

        return surgical;
    }

    @Override
    public Medication findNextMedicationByID(Medication medication) throws SQLException {
        String queryNext = "SELECT PATIENTID,DATEOFMED,MED,UNITCOST,UNITS FROM MEDICATION WHERE PATIENTID = (SELECT MIN(PATIENTID) FROM MEDICATION WHERE PATIENTID > ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(queryNext);) {
            pStatement.setInt(1, medication.getPatientiD());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundMedicationData(resultSet, medication);
                }
            }
        }
        log.info("Found " + medication.getPatientiD() + "???: " + (medication != null));

        return medication;
    }

    @Override
    public Medication findPreviousMedicationtByID(Medication medication) throws SQLException {
        String queryNext = "SELECT PATIENTID,DATEOFMED,MED,UNITCOST,UNITS FROM MEDICATION WHERE PATIENTID = (SELECT MAX(PATIENTID) FROM MEDICATION WHERE PATIENTID < ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(queryNext);) {
            pStatement.setInt(1, medication.getPatientiD());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundMedicationData(resultSet, medication);
                }
            }
        }
        log.info("Found " + medication.getPatientiD() + "???: " + (medication != null));

        return medication;
    }

    private Patient createBoundPatientData(ResultSet rSet, Patient patient) throws SQLException {
        patient.setId(rSet.getInt("PATIENTID"));
        patient.setLastName(rSet.getString("LASTNAME"));
        patient.setFirstName(rSet.getString("FIRSTNAME"));
        patient.setDiagnosis(rSet.getString("DIAGNOSIS"));
        patient.setAdmissionDate(rSet.getString("ADMISSIONDATE"));
        patient.setReleaseDate(rSet.getString("RELEASEDATE"));
        return patient;
    }

    private Inpatient createBoundInpatientData(ResultSet rSet, Inpatient inpatient) throws SQLException {

        inpatient.setInpatientID(rSet.getInt("PATIENTID"));
        inpatient.setDateOfStay(rSet.getString("DATEOFSTAY"));
        inpatient.setRoomNumber(rSet.getString("ROOMNUMBER"));
        inpatient.setdailyRate(rSet.getDouble("DAILYRATE"));
        inpatient.setSupplies(rSet.getDouble("SUPPLIES"));
        inpatient.setServices(rSet.getDouble("SERVICES"));
        return inpatient;
    }

    private Surgical createBoundSurgicalData(ResultSet rSet, Surgical surgical) throws SQLException {

        surgical.setPatient_id(rSet.getInt("PATIENTID"));
        surgical.setDateOfSurgery(rSet.getString("DATEOFSURGERY"));
        surgical.setSurgery(rSet.getString("SURGERY"));
        surgical.setRoomFee(rSet.getDouble("ROOMFEE"));
        surgical.setSurgeOnFee(rSet.getDouble("SURGEONFEE"));
        surgical.setSupplies(rSet.getDouble("SUPPLIES"));
        return surgical;
    }

    private Medication createBoundMedicationData(ResultSet rSet, Medication medication) throws SQLException {

        medication.setPatientiD(rSet.getInt("PATIENTID"));
        medication.setDateOfMed(rSet.getString("DATEOFMED"));
        medication.setMed(rSet.getString("MED"));
        medication.setUnitCost(rSet.getDouble("UNITCOST"));
        medication.setMedUnits(rSet.getDouble("UNITS"));
        return medication;
    }

    private BillReport createBillReportData(ResultSet rSet) throws SQLException {

        BillReport bill1 = new BillReport();

        bill1.setPatientID(rSet.getInt("PATIENTID"));
        bill1.setLastName(rSet.getString("LASTNAME"));
        bill1.setFirstName(rSet.getString("FIRSTNAME"));
        bill1.setRoomNumber(rSet.getString("ROOMNUMBER"));
        bill1.setDailyRate(rSet.getString("DAILYRATE"));
        bill1.setSupplies(rSet.getString("SUPPLIES"));
        bill1.setServices(rSet.getString("SERVICES"));
        bill1.setRoomFee(rSet.getString("ROOMFEE"));
        bill1.setSurgeOnFee(rSet.getString("SURGEONFEE"));
        bill1.setMedication(rSet.getString("MEDICATION"));
        return bill1;

    }

    private BillReport createBillTotalData(ResultSet rSet) throws SQLException {

        BillReport bill1 = new BillReport();

        bill1.setTotal(rSet.getString("TOTAL"));

        return bill1;

    }

}
