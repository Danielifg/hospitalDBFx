package com.danielFlores.hospitalDB.persistence.daoDeclaration;

import com.danielFlores.hospitalDB.beans.Inpatient;
import com.danielFlores.hospitalDB.beans.Medication;
import com.danielFlores.hospitalDB.beans.Patient;
import com.danielFlores.hospitalDB.beans.Surgical;
import java.sql.SQLException;

/**
 *
 * @author Daniel
 *
 * Data Access Object Declaration_CREATE.
 */
public interface HospitalDAO_create {

    public int createPatient(Patient patient) throws SQLException;

    public int createInpatient(Inpatient inpatient) throws SQLException;

    public int createMedication(Medication medication) throws SQLException;

    public int createSurgical(Surgical surgical) throws SQLException;

}
