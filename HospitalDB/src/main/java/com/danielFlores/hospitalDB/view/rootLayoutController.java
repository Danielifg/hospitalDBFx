package com.danielFlores.hospitalDB.view;

import com.danielFlores.bill.view.billController;
import com.danielFlores.surgical.view.surgicalController;
import com.danielFlores.hospitalApp.mainApp;
import com.danielFlores.medication.view.medicationController;
import com.danielFlores.inpatient.view.inpatientController;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_read;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import com.danielFlores.patient.view.patientController;
import com.danielFlores.hospitalDB.persistence.daoDeclaration.HospitalDAO_create;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_create;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_delete;
import com.danielFlores.hospitalDB.persistence.daoImplementation.HospitalDB_update;

/**
 *
 * @author Daniel
 *
 * Root Scene Class to load every xml file for each POJO.
 *
 */
public class rootLayoutController {

    @FXML
    private AnchorPane patientAnchor;

    @FXML
    private AnchorPane inpatientAnchor;

    @FXML
    private AnchorPane surgicalAnchor;

    @FXML
    private AnchorPane medicationAnchor;

    @FXML
    private AnchorPane billScene;

    @FXML
    private AnchorPane dbAnchor;

    @FXML
    private ResourceBundle resources;

    private final HospitalDB_create dao_create;
    private final HospitalDB_delete dao_delete;
    private final HospitalDB_read   dao_read;
    private final HospitalDB_update dao_update;

    private patientController patientController;
    private inpatientController inpatientController;
    private surgicalController surgicalController;
    private medicationController medicationController;
    private billController billController;

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    public rootLayoutController() {
        super();
        log.info("test");
        
        dao_create  = new HospitalDB_create();
        dao_read    = new HospitalDB_read();
        dao_update  = new HospitalDB_update();
        dao_delete  = new HospitalDB_delete();
        

    }

    @FXML
    private void initialize() {

        initPatientTab();
        initInpatientTab();
        initSurgicalTab();
        initMedicationTab();
        initBillTab();

        try {
//       patientController.setHospitalDAO(hospitalDAO);
            patientController.displayTheTable();
            inpatientController.displayTheTable();
            surgicalController.displayTheTable();
            medicationController.displayTheTable();

            //
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void initPatientTab() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(mainApp.class.getResource("/fxml/patient.fxml"));
            AnchorPane patientView = (AnchorPane) loader.load();

            patientController = loader.getController();
            patientController.setHospitalDAO(dao_read);
            patientAnchor.getChildren().add(patientView);

        } catch (IOException | SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void initInpatientTab() {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(mainApp.class.getResource("/fxml/inpatient.fxml"));
            AnchorPane inpatientView = (AnchorPane) loader.load();

            inpatientController = loader.getController();
            inpatientController.setHospitalDAO(dao_read);

            inpatientAnchor.getChildren().add(inpatientView);

        } catch (IOException | SQLException ex) {
            ex.printStackTrace();

        }

    }

    public void initSurgicalTab() {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(mainApp.class.getResource("/fxml/surgical.fxml"));
            AnchorPane surgicaltView = (AnchorPane) loader.load();

            surgicalController = loader.getController();
            surgicalController.setHospitalDAO(dao_read);

            surgicalAnchor.getChildren().add(surgicaltView);

        } catch (IOException | SQLException ex) {
            ex.printStackTrace();

        }

    }

    public void initMedicationTab() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(mainApp.class.getResource("/fxml/medication.fxml"));
            AnchorPane medicationView = (AnchorPane) loader.load();

            medicationController = loader.getController();
            medicationController.setHospitalDAO(dao_read);

            medicationAnchor.getChildren().add(medicationView);

        } catch (IOException | SQLException ex) {
            ex.printStackTrace();

        }
    }

    public void initBillTab() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(mainApp.class.getResource("/fxml/bill.fxml"));
            AnchorPane billView = (AnchorPane) loader.load();

            billController = loader.getController();
            billController.setDao_read(dao_read);
            billController.setDao_create(dao_create);
            billController.setDao_delete(dao_delete);
            billController.setDao_update(dao_update);
            billScene.getChildren().add(billView);

        } catch (IOException ex) {
            ex.printStackTrace();

        }
    }
}
